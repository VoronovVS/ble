package com.vk.bleapi;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Voronov Viacheslav
 * on 5/3/2016
 * 10:40 AM
 */
public class UserNearby {

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("device_id")
    private String deviceId;

    private String id;

    @SerializedName("mac_address")
    private String macAddress;

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getFullName(){
        return firstName + " " + lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
