package com.vk.bleapi;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 4/17/2016
 * 9:01 PM
 */
public class BleUtils {

    public static boolean isBleSupported(Context context){
        PackageManager pm = context.getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    public static BluetoothAdapter getBluetoothAdapter(Context context){
        BluetoothManager manager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        return manager.getAdapter();
    }

    public static boolean enabled(Context context) {
        BluetoothAdapter bluetoothAdapter = getBluetoothAdapter(context);
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static boolean isPeripheralModeSupported(Context context){
        if(!checkLollipop()){
            return false;
        }

        BluetoothAdapter adapter = getBluetoothAdapter(context);
        return adapter.isMultipleAdvertisementSupported() && adapter.isOffloadedFilteringSupported() && adapter.isOffloadedScanBatchingSupported();
    }

    public static boolean checkLollipop(){
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static boolean checkMarshmallow(){
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static UUID asUuid(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        long firstLong = bb.getLong();
        long secondLong = bb.getLong();
        return new UUID(firstLong, secondLong);
    }

    public static byte[] asBytes(UUID uuid) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        return bb.array();
    }

    public static byte[] coordinateAsBytes(double latitude, double longitude) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putDouble(latitude);
        bb.putDouble(longitude);
        return bb.array();
    }

}
