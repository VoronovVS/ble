package com.vk.bleapi;

import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 4/18/2016
 * 12:45 AM
 */
public class BleDevice {

    private static final long CONNECTION_TIMEOUT = 10000;

    protected String name;
    protected UUID uuid;
    protected long synchronizationTime = System.currentTimeMillis();
    protected String address;
    protected String manufacturer;
    private long connectionTime = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public long getSynchronizationTime() {
        return synchronizationTime;
    }

    public void setSynchronizationTime(long synchronizationTime) {
        this.synchronizationTime = synchronizationTime;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof BleDevice && address != null && address.equals(((BleDevice)o).address);
    }

    @Override
    public int hashCode() {
        if (address == null){
            return 0;
        } else {
            return address.hashCode();
        }
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isConnecting() {
        return connectionTime != 0 && System.currentTimeMillis() - connectionTime <= CONNECTION_TIMEOUT;
    }

    public void setConnecting(boolean connecting) {
        connectionTime = connecting ? System.currentTimeMillis() : 0;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
}
