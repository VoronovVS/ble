package com.vk.bleapi;

/**
 * Created by Voronov Viacheslav
 * on 4/27/2016
 * 11:53 PM
 */
public interface BleConstants {

    int BLUETOOTH_IS_DISABLED_ERROR = 11;
    int BLUETOOTH_NO_RESULTS_ERROR = 12;

}
