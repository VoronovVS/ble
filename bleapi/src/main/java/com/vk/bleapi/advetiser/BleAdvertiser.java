package com.vk.bleapi.advetiser;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import com.vk.bleapi.BleUtils;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 4/21/2016
 * 2:13 AM
 */

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class BleAdvertiser {

    private static final String LOGS = "BleAdvertiser";

    private BluetoothManager bluetoothManager;
    private BluetoothLeAdvertiser bluetoothLeAdvertiser;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothGattServer gattServer;
    private AdvertiserCallback callback;

    private Context context;
    private BleAdvertiserListener listener;
    private boolean isRunning;

    private AdvertiseInfo info;

    public BleAdvertiser(Context context) {
        this.context = context;

        bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        initBluetooth();
    }

    private void initBluetooth(){
        bluetoothAdapter = bluetoothManager.getAdapter();
        bluetoothLeAdvertiser = bluetoothAdapter.getBluetoothLeAdvertiser();
    }

    public void setListener(BleAdvertiserListener listener){
        this.listener = listener;
    }

    public void setInfo(AdvertiseInfo info){
        this.info = info;
    }

    public void launch() {

        if (!BleUtils.checkLollipop()){
            listener.onAdvertiseFail(AdvertiseCallback.ADVERTISE_FAILED_FEATURE_UNSUPPORTED);
            return;
        }

        if(bluetoothLeAdvertiser == null){
            initBluetooth();

            if (bluetoothLeAdvertiser == null){
                listener.onAdvertiseFail(AdvertiseCallback.ADVERTISE_FAILED_FEATURE_UNSUPPORTED);
                return;
            }
        }

        AdvertiseSettings settings = buildAdvertiseSettings();
        AdvertiseData data = buildAdvertiseData();

        callback = new AdvertiserCallback();
        bluetoothLeAdvertiser.startAdvertising(settings, data, callback);

        isRunning = true;
    }

    public void stop() {
        Log.d(LOGS,"Stopping advertising...");
        try {
            bluetoothLeAdvertiser.stopAdvertising(callback);
        } catch (Exception ex){
            ex.printStackTrace();
        }

        isRunning = false;
    }

    public boolean isRunning() {
        return callback != null && isRunning;
    }

    public boolean updateInfo(){
        if (!isRunning){
            throw new IllegalStateException("Unable to update characteristic: gatt is not running");
        } else if (info == null){
            throw new IllegalStateException("Unable to update characteristic: info wasn't set");
        } else if (gattServer == null){
            listener.onAdvertiseFail(AdvertiseCallback.ADVERTISE_FAILED_INTERNAL_ERROR);
            return false;
        }

        BluetoothGattService service = gattServer.getService(info.getServiceUuid().getUuid());
        if (service == null){
            throw new IllegalStateException("Unable to update characteristic: service is not found");
        }

        gattServer.removeService(service);
        addDeviceInfoService();

        return false;
    }

    private AdvertiseSettings buildAdvertiseSettings() {
        AdvertiseSettings.Builder settingsBuilder = new AdvertiseSettings.Builder();
        settingsBuilder.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY);
        settingsBuilder.setTimeout(0);
        settingsBuilder.setConnectable(true);
        settingsBuilder.setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM);
        return settingsBuilder.build();
    }

    private AdvertiseData buildAdvertiseData() {
        bluetoothAdapter.setName(info.getName());
        return new AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .addServiceUuid(info.getServiceUuid())
                .build();
    }

    private void startGattServer(){
        gattServer = bluetoothManager.openGattServer(context, gattServerCallback);
        if (gattServer == null) {
            listener.onAdvertiseFail(AdvertiseCallback.ADVERTISE_FAILED_INTERNAL_ERROR);
        } else {
            addDeviceInfoService();
        }
    }

    private void addDeviceInfoService() {

        BluetoothGattService deviceInfoService = new BluetoothGattService(
                info.getServiceUuid().getUuid(),
                BluetoothGattService.SERVICE_TYPE_PRIMARY);

        for (Map.Entry<UUID, byte[]> entry : info.getData().entrySet()){
            BluetoothGattCharacteristic characteristic = new BluetoothGattCharacteristic(
                    entry.getKey(), BluetoothGattCharacteristic.PROPERTY_READ, BluetoothGattCharacteristic.PERMISSION_READ);
            characteristic.setValue(entry.getValue());
            deviceInfoService.addCharacteristic(characteristic);
            Log.d(LOGS, "Writting " + entry.getKey() + " with value '" + new String(entry.getValue()) + "'");
        }

        gattServer.addService(deviceInfoService);
    }

    private class AdvertiserCallback extends AdvertiseCallback {

        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            startGattServer();
            listener.onAdvertiseSuccess();
        }

        @Override
        public void onStartFailure(int errorCode) {
            listener.onAdvertiseFail(errorCode);
        }

    }

    private BluetoothGattServerCallback gattServerCallback = new BluetoothGattServerCallback() {

        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            super.onConnectionStateChange(device, status, newState);

            checkDevice(device);
        }

        @Override
        public void onCharacteristicReadRequest(final BluetoothDevice device, int requestId, int offset, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic);
            Log.i(LOGS, "MyBluetoothGattServerCallback.onCharacteristicReadRequest from " + device.getName() + " / " + device.getAddress());

            if (!checkDevice(device)){
                return;
            }

            byte[] src = characteristic.getValue();
            int len = src.length - offset;
            byte[] dest = new byte[len];
            System.arraycopy(src, offset, dest, 0, len);
            src = dest;

            Log.d(LOGS, String.format("offset %d length %d data: %s", offset, len, new String(src)));
            if (gattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, src)) {
                Log.d(LOGS, "Characteristic " + characteristic.getUuid().toString() + " has been sent");
            }
        }
    };

    private boolean checkDevice(BluetoothDevice device){
        synchronized (connectedDevices) {
            ConnectedDevice connectedDevice = connectedDevices.get(device.getAddress());
            if (connectedDevice == null) {
                connectedDevice = new ConnectedDevice();
                connectedDevice.address = device.getAddress();
                connectedDevice.device = new WeakReference<>(device);
                connectedDevices.put(device.getAddress(), connectedDevice);
                scheduleDisconnectTask(device);
                Log.d(LOGS, "Newly connected device: " + connectedDevice.address);
                return true;
            } else if (System.currentTimeMillis() - connectedDevice.initTime < ConnectedDevice.ENABLED) {
                Log.d(LOGS, "Recent device. Connection allowed: " + connectedDevice.address);
                return true;
            } else if (System.currentTimeMillis() - connectedDevice.initTime > ConnectedDevice.BAN_TIMEOUT) {
                connectedDevice.initTime = System.currentTimeMillis();
                scheduleDisconnectTask(device);
                Log.d(LOGS, "Old device. Connection allowed. " + connectedDevice.address);
                return true;
            } else {
                gattServer.cancelConnection(device);
                Log.d(LOGS, "Refusing connection for device: " + connectedDevice.address);
                return false;
            }
        }
    }

    private void scheduleDisconnectTask(final BluetoothDevice device){
        handler.postDelayed(() -> {
            ConnectedDevice connected = connectedDevices.get(device.getAddress());
            if (gattServer != null && connected != null && connected.device.get() != null){
                gattServer.cancelConnection(connected.device.get());
                Log.d(LOGS, "Disconnecting from device by timeout: " + device.getAddress());
            }
        }, ConnectedDevice.ENABLED);

    }

    private Handler handler = new Handler();
    private final Map<String, ConnectedDevice> connectedDevices = new HashMap<>();

    private class ConnectedDevice {

        static final long BAN_TIMEOUT = 20000;
        static final long ENABLED = 10000;

        long initTime = System.currentTimeMillis();
        WeakReference<BluetoothDevice> device;
        String address;

    }

}
