package com.vk.bleapi.advetiser;

import android.os.ParcelUuid;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 4/21/2016
 * 2:22 AM
 */
public class AdvertiseInfo {

    private Map<UUID, byte[]> data = new HashMap<>();
    private UUID serviceUuid;
    private String name;

    public Map<UUID, byte[]> getData() {
        return data;
    }

    public void addServiceData(UUID uuid, byte[] bytes) {
        data.put(uuid, bytes);
    }

    public ParcelUuid getServiceUuid() {
        return new ParcelUuid(serviceUuid);
    }

    public void setServiceUuid(UUID serviceUuid) {
        this.serviceUuid = serviceUuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
