package com.vk.bleapi.advetiser;

/**
 * Created by Voronov Viacheslav
 * on 4/17/2016
 * 10:12 PM
 */
public interface BleAdvertiserListener {

    void onAdvertiseSuccess();
    void onAdvertiseFail(int reason);

}
