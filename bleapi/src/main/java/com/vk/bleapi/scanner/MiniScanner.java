package com.vk.bleapi.scanner;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

import com.google.gson.Gson;
import com.vk.bleapi.BleDevice;
import com.vk.bleapi.BleUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 5/24/2016
 * 7:12 PM
 */
public class MiniScanner implements BleScanner {

    private static final String LOGS = "BBleScanner";
    protected static final long SCAN_FREQUENCY = 8000;
    private static final long SCAN_TIME = 4000;
    protected static final long CONNECTION_INIT_FREQUENCY = 3000;
    protected static final long SCAN_RESULT_LIFE_TIME = 22000;
    protected static final long CLEANING_FREQUENCY = 9000;

    private static final String BLE_SERVICE_KEY = "B206EE5D-17EE-40C1-92BA-462A038A33D2"; //fixme : remove

    private Context context;
    private BluetoothLeScanner scanner;
    private Handler handler = new Handler();
    private BluetoothAdapter adapter;

    private final Map<String, BleDevice> devices = new HashMap<>();
    //private Set<String> ignoredDevices = new HashSet<>();
    private final Map<String, BluetoothDevice> discoveredDevices = new HashMap<>();
    private BleScannerListener listener;

    private boolean isRunning;

    public MiniScanner(Context context){
        this.context = context;
        this.adapter = BleUtils.getBluetoothAdapter(context);
        this.scanner = adapter.getBluetoothLeScanner();
    }

    @Override
    public void launch() {
        if(isRunning){
            return;
        }

        if(scanner == null) {
            this.scanner = adapter.getBluetoothLeScanner();
            if (scanner == null) {
                listener.onScanFail(ScanCallback.SCAN_FAILED_INTERNAL_ERROR);
                return;
            }
        }

        startScan();

        if (listener != null){
            listener.onScanLaunched();
        }

        launchPeriodicTasks();

        isRunning = true;
    }

    @Override
    public void stop() {
        devices.clear();
        stopPeriodicTasks();
        cancelCurrentScan();
        isRunning = false;

        if (listener != null){
            listener.onScanStopped();
        }
    }

    @Override
    public void pause() {
        stopPeriodicTasks();
        cancelCurrentScan();
        isRunning = false;
    }

    @Override
    public void resume() {
        launchPeriodicTasks();
        isRunning = true;
    }

    protected void cancelCurrentScan() {
        if (adapter.isEnabled() && scanner != null) {
            scanner.stopScan(scanCallback);
        }
    }

    @Override
    public void setListener(BleScannerListener listener) {
        this.listener = listener;
    }

    @Override
    public void setServiceUuidFilter(UUID uuid) {

    }

    @Override
    public void connectToDevices(List<String> devices) {

    }

    @Override
    public void onDeviceInfoReceived(String address, String userId, UUID uuid) {
        synchronized (discoveredDevices) {

            for (Map.Entry<String, BleDevice> pair: devices.entrySet()){
                BleDevice bleDevice = pair.getValue();
                if ((bleDevice.getAddress() != null && bleDevice.getAddress().equals(address)) || ((bleDevice.getName() != null && bleDevice.getName().equals(userId)))){
                    bleDevice.setUuid(uuid);
                    bleDevice.setName(userId);

                    discoveredDevices.remove(address);

                    if (listener != null) {
                        listener.onScanUpdate(bleDevice);
                    }
                }
            }
        }
    }

    @Override
    public Map<String, BleDevice> getCurrentDevices() {
        return devices;
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    private void startScan(){
        Log.d(LOGS, "Scanner is scanning...");
        scanner.startScan(getScanFilters(), getScanSettings(), scanCallback);
        handler.postDelayed(scannerStopTask, SCAN_TIME);
    }

    protected ScanSettings getScanSettings(){
        ScanSettings.Builder b = new ScanSettings.Builder();
        b.setScanMode(ScanSettings.SCAN_MODE_BALANCED);
        return b.build();
    }

    protected List<ScanFilter> getScanFilters(){
        ScanFilter.Builder sfb = new ScanFilter.Builder();
        // sfb.setServiceUuid(new ParcelUuid(UUID.fromString("B206EE5D-17EE-40C1-92BA-462A038A33D2")));
        List<ScanFilter> filter = new ArrayList<>();
        filter.add(sfb.build());
        return filter;
    }

    private ScanCallback scanCallback = new ScanCallback() {

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            foundDevice(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            for (ScanResult scanResult : results) {
                foundDevice(scanResult);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            if (listener != null) {
                listener.onScanFail(errorCode);
            }
        }
    };

    private Runnable scannerStopTask = new Runnable() {
        @Override
        public void run() {
            if (scanner != null && adapter.isEnabled()){
                Log.d(LOGS, "Scanner is resting...");
                scanner.stopScan(scanCallback);
            }
        }
    };

    private void launchPeriodicTasks() {
        handler.postDelayed(cleaningTask, CLEANING_FREQUENCY);
        handler.postDelayed(periodicScanTask, SCAN_FREQUENCY);
        handler.postDelayed(periodicConnectTask, 1000);
    }

    private void stopPeriodicTasks(){
        handler.removeCallbacks(cleaningTask);
        handler.removeCallbacks(periodicScanTask);
        handler.removeCallbacks(periodicConnectTask);
    }

    Runnable cleaningTask = new Runnable() {
        @Override
        public void run() {
            removeOldScanResults();
            handler.postDelayed(this, SCAN_FREQUENCY);
        }
    };

    Runnable periodicScanTask = new Runnable() {
        @Override
        public void run() {
            restartScan();
            handler.postDelayed(this, SCAN_FREQUENCY);
        }
    };

    Runnable periodicConnectTask = new Runnable() {
        @Override
        public void run() {
            sendOnDeviceDiscoveredBroadcast();
            handler.postDelayed(this, CONNECTION_INIT_FREQUENCY);
        }
    };

    private synchronized void removeOldScanResults() {
        Iterator<Map.Entry<String, BleDevice>> iterator = devices.entrySet().iterator();

        long currentTime = System.currentTimeMillis();

        while (iterator.hasNext()) {
            Map.Entry<String, BleDevice> pair = iterator.next();
            BleDevice device = pair.getValue();
            boolean remove = false;
            synchronized (devices) {
                if (currentTime - device.getSynchronizationTime() > SCAN_RESULT_LIFE_TIME) {
                    iterator.remove();
                    remove = true;
                }
            }

            if (remove && listener != null) {
                listener.onDeviceLost(device);
            }
        }
    }

    private void sendOnDeviceDiscoveredBroadcast(){ //todo refactor
        if (discoveredDevices.size() == 0){
            return;
        }

        List<BleDevice> devices;
        synchronized (discoveredDevices) {

            devices = new ArrayList<>();
            for (Map.Entry<String, BleDevice> pair : this.devices.entrySet()) {
                BleDevice device = pair.getValue();
                if (device.getUuid() == null){
                    devices.add(device);
                }
            }
        }

        Intent intent = new Intent(ACTION_ON_NEW_DEVICES_DISCOVERED);
        String json = new Gson().toJson(devices);        //// FIXME: 5/19/2016 remove GSON
        intent.putExtra(EXTRA_DEVICE_ADDRESSES, json);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    protected void restartScan() {
        if(adapter.isEnabled()) {
            if (scanner == null){
                this.scanner = adapter.getBluetoothLeScanner();

                if (scanner == null){
                    stop();
                }
            }
            scanner.stopScan(scanCallback);
            startScan();
        } else {
            if (listener != null) {
                listener.onScanFail(ScanCallback.SCAN_FAILED_INTERNAL_ERROR);
            }
        }
    }

    private void foundDevice(ScanResult result){

        BluetoothDevice device = result.getDevice();

        Log.d(LOGS, "On device found " + device.getAddress());

        BleDevice bleDevice = devices.get(device.getAddress());
        if (bleDevice == null){
            bleDevice = new BleDevice();
            bleDevice.setAddress(device.getAddress());

            if (result.getScanRecord() != null && !TextUtils.isEmpty(result.getScanRecord().getDeviceName())){
                devices.put(device.getAddress(), bleDevice);
            } else {
                return;
            }
        }

        if (result.getScanRecord() != null && !TextUtils.isEmpty(result.getScanRecord().getDeviceName())){
            bleDevice.setName(result.getScanRecord().getDeviceName());
        }

        bleDevice.setSynchronizationTime(System.currentTimeMillis());

        if (listener != null) {
            listener.onScanUpdate(bleDevice);
        }
    }
}
