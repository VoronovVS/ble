package com.vk.bleapi.scanner;

import android.annotation.TargetApi;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Voronov Viacheslav
 * on 4/17/2016
 * 10:17 PM
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class ModernBleScanner extends BaseBleScanner  {

    private static final long SCAN_TIME = 4000;

    private BluetoothLeScanner scanner;
    private Handler handler = new Handler();

    public ModernBleScanner(Context context){
        super(context);
        this.scanner = getAdapter().getBluetoothLeScanner();
    }

    @Override
    public void launch() {
        if(scanner == null) {
            this.scanner = getAdapter().getBluetoothLeScanner();
            if (scanner == null) {
                getListener().onScanFail(ScanCallback.SCAN_FAILED_INTERNAL_ERROR);
                return;
            }
        }

        super.launch();

        startScan();

        if (getListener() != null){
            getListener().onScanLaunched();
        }
    }

    @Override
    public void stop() {
        super.stop();

        if(scanner == null) {
            this.scanner = getAdapter().getBluetoothLeScanner();
        }

        if (getAdapter().isEnabled() && scanner != null) {
            scanner.stopScan(scanCallback);
            handler.removeCallbacks(scannerStopTask);
        }

        getListener().onScanStopped();
        Log.d(LOGS, "Scanner was stopped...");
    }

    private void startScan(){

        Log.d(LOGS, "Scanner is scanning...");
        scanner.startScan(getScanFilters(), getScanSettings(), scanCallback);
        handler.postDelayed(scannerStopTask, SCAN_TIME);
    }

    @Override
    protected void cancelCurrentScan() {
        if (getAdapter().isEnabled()) {
            scanner.stopScan(scanCallback);
        }
    }

    private Runnable scannerStopTask = new Runnable() {
        @Override
        public void run() {
            if (scanner != null && getAdapter().isEnabled()){
                Log.d(LOGS, "Scanner is resting...");
                scanner.stopScan(scanCallback);
            }
        }
    };

    @Override
    protected void restartScan() {
        if(getAdapter().isEnabled()) {
            if (scanner == null){
                this.scanner = getAdapter().getBluetoothLeScanner();

                if (scanner == null){
                    stop();
                }
            }
            scanner.stopScan(scanCallback);
            startScan();
        } else {
            getListener().onScanFail(ScanCallback.SCAN_FAILED_INTERNAL_ERROR );
        }
    }

    protected ScanSettings getScanSettings(){
        ScanSettings.Builder b = new ScanSettings.Builder();
        b.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
        return b.build();
    }

    protected List<ScanFilter> getScanFilters(){
        ScanFilter.Builder sfb = new ScanFilter.Builder();
       // sfb.setServiceUuid(new ParcelUuid(UUID.fromString("B206EE5D-17EE-40C1-92BA-462A038A33D2")));
        List<ScanFilter> filter = new ArrayList<>();
        filter.add(sfb.build());
        return filter;
    }

    private ScanCallback scanCallback = new ScanCallback() {

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            foundDevice(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            for (ScanResult scanResult : results) {
                foundDevice(scanResult);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            getListener().onScanFail(errorCode);
        }
    };

}
