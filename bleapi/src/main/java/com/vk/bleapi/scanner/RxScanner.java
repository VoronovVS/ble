package com.vk.bleapi.scanner;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.polidea.rxandroidble.RxBleClient;
import com.polidea.rxandroidble.RxBleConnection;
import com.polidea.rxandroidble.RxBleDevice;
import com.polidea.rxandroidble.RxBleScanResult;
import com.vk.bleapi.BleDevice;
import com.vk.bleapi.BleUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by Voronov Viacheslav
 * on 5/18/2016
 * 9:34 AM
 */
public class RxScanner implements BleScanner {

    private static final String LOGS = "RxScan";
    private static final long CLEANING_FREQUENCY = 9000;
    private static final long SCAN_RESULT_LIFE_TIME = 22000;
    private static final long SCAN_FREQUENCY = 8000;
    private static final long CONNECTION_INIT_FREQUENCY = 3000;

    private static final UUID BLE_UUID_CHARACTERISTIC = UUID.fromString("E669893C-F4C2-4604-800A-5252CED237F9"); //fixme : remove
    private static final UUID BLE_NAME_CHARACTERISTIC = UUID.fromString("2EFDAD55-5B85-4C78-9DE8-07884DC051FA"); //fixme : remove

    private Context context;
    private RxBleClient bleClient;
    private Subscription scanSubscription;
    private BleScannerListener listener;
    private Handler handler = new Handler();
    private Thread connectionThread;

    private final Map<String, BleDevice> devices = new HashMap<>();
    private final Queue<RxBleDevice> connectionQueue = new LinkedList<>();
    private final List<String> ignoredDevices = new ArrayList<>();

    private boolean isConnecting;
    private boolean canProceedConnection = true;


    public RxScanner(Context context, RxBleClient bleClient){
        this.context = context;
        this.bleClient = bleClient;
    }

    @Override
    public void launch() {
        scanSubscription = bleClient.scanBleDevices()
                .doOnUnsubscribe(() -> scanSubscription = null)
                .subscribe(new BleScanResultSubscriber());
        canProceedConnection = true;
        isConnecting = false;
        launchPeriodicTasks();
    }

    @Override
    public void stop() {
        scanSubscription.unsubscribe();
        stopPeriodicTasks();

        devices.clear();
        connectionQueue.clear();
    }

    @Override
    public void pause() {
        scanSubscription.unsubscribe();
        stopPeriodicTasks();
    }

    @Override
    public void resume() {
        scanSubscription = bleClient.scanBleDevices()
                .doOnUnsubscribe(() -> scanSubscription = null)
                .subscribe(new BleScanResultSubscriber());
        launchPeriodicTasks();
    }

    private class BleScanResultSubscriber extends Subscriber<RxBleScanResult> {
        @Override
        public void onCompleted() {
            Log.d(LOGS, "On scan finish");
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
        }

        @Override
        public void onNext(RxBleScanResult rxBleScanResult) {

            Log.d(LOGS, "On device discovered: " + rxBleScanResult.getBleDevice().getMacAddress());
            BleDevice device = devices.get(rxBleScanResult.getBleDevice().getMacAddress());

            if (device == null){
                device = new BleDevice();
                device.setAddress(rxBleScanResult.getBleDevice().getMacAddress());
                devices.put(rxBleScanResult.getBleDevice().getMacAddress(), device);
                connectionQueue.add(rxBleScanResult.getBleDevice());
                listener.onScanUpdate(device);
            }

            device.setSynchronizationTime(System.currentTimeMillis());
        }
    }

    private void launchPeriodicTasks() {
        handler.postDelayed(periodicConnectTask, 2000);
        handler.postDelayed(cleaningTask, CLEANING_FREQUENCY);
        handler.postDelayed(periodicScanTask, SCAN_FREQUENCY);
    }

    private Runnable periodicConnectTask = new Runnable() {
        @Override
        public void run() {
            connectToDevices();
            handler.postDelayed(this, CONNECTION_INIT_FREQUENCY);
        }
    };

    private Runnable cleaningTask = new Runnable() {
        @Override
        public void run() {
            removeOldScanResults();
            handler.postDelayed(this, SCAN_FREQUENCY);
        }
    };

    private Runnable periodicScanTask = new Runnable() {
        @Override
        public void run() {
            scanSubscription = bleClient.scanBleDevices()
                    .doOnUnsubscribe(() -> scanSubscription = null)
                    .subscribe(new BleScanResultSubscriber());

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (scanSubscription != null) {
                        scanSubscription.unsubscribe();
                    }
                }
            }, 4000);

            handler.postDelayed(this, SCAN_FREQUENCY);
        }
    };

    private void stopPeriodicTasks(){
        handler.removeCallbacks(periodicConnectTask);
        handler.removeCallbacks(cleaningTask);
        handler.removeCallbacks(periodicScanTask);
    }

    private synchronized void removeOldScanResults() {
        Iterator<Map.Entry<String, BleDevice>> iterator = devices.entrySet().iterator();

        long currentTime = System.currentTimeMillis();

        while (iterator.hasNext()) {
            Map.Entry<String, BleDevice> pair = iterator.next();
            BleDevice device = pair.getValue();
            boolean remove = false;
            synchronized (devices) {
                if (currentTime - device.getSynchronizationTime() > SCAN_RESULT_LIFE_TIME) {
                    iterator.remove();
                    remove = true;
                }
            }

            if (remove && listener != null) {
                listener.onDeviceLost(device);
            }
        }
    }

    protected void connectToDevices(){

        if (isConnecting || connectionQueue.size() == 0){ return; }

        isConnecting = true;

        if(connectionThread == null){
            connectionThread = new Thread(() -> {
                connectionLoop();
                connectionThread.interrupt();
                connectionThread = null;
            });

            connectionThread.start();
        }
    }

    private void connectionLoop(){
        while(!connectionQueue.isEmpty()){

            if (!canProceedConnection){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {}
                return;
            }

            RxBleDevice device = connectionQueue.poll();
            if (device == null){
                continue;
            }

            canProceedConnection = false;
            device.observeConnectionStateChanges().subscribe(new Subscriber<RxBleConnection.RxBleConnectionState>() {
                @Override
                public void onCompleted() {
                    Log.d(LOGS, "observeConnectionStateChanges onCompleted");
                }

                @Override
                public void onError(Throwable e) {
                    Log.d(LOGS, "observeConnectionStateChanges onError");
                    e.printStackTrace();
                }

                @Override
                public void onNext(RxBleConnection.RxBleConnectionState rxBleConnectionState) {
                    Log.d(LOGS, "observeConnectionStateChanges onNext " + rxBleConnectionState.toString());
                    if (RxBleConnection.RxBleConnectionState.DISCONNECTED == rxBleConnectionState){
                        if(listener != null){
                            BleDevice bleDevice = devices.get(device.getMacAddress());
                            if (bleDevice != null) {
                                listener.onDeviceLost(bleDevice);
                                devices.remove(bleDevice.getAddress());
                            }
                        }
                    }
                }
            });
            device.establishConnection(context, false)
                    .flatMap(rxBleConnection -> Observable.combineLatest(
                            rxBleConnection.readCharacteristic(BLE_UUID_CHARACTERISTIC),
                            rxBleConnection.readCharacteristic(BLE_NAME_CHARACTERISTIC),
                            Characteristics::new
                    ))
                    .subscribe(new Subscriber<Characteristics>() {
                                   @Override
                                   public void onCompleted() {
                                       canProceedConnection = true;
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       e.printStackTrace();
                                   }

                                   @Override
                                   public void onNext(Characteristics characteristics) {
                                       BleDevice bleDevice = devices.get(device.getMacAddress());
                                       if (bleDevice != null){
                                           bleDevice.setUuid(characteristics.uuid);
                                           bleDevice.setName(characteristics.name);
                                           listener.onScanUpdate(bleDevice);
                                       }
                                   }
                               }

                    );
        }

        isConnecting = false;
    }

    @Override
    public void setListener(BleScannerListener listener) {
        this.listener = listener;
    }

    @Override
    public void setServiceUuidFilter(UUID uuid) {
        //todo
    }

    @Override
    public void connectToDevices(List<String> devices) {

    }

    @Override
    public void onDeviceInfoReceived(String address, String userId, UUID uuid) {

    }

    @Override
    public Map<String, BleDevice> getCurrentDevices() {
        return devices;
    }

    @Override
    public boolean isRunning() {
        return scanSubscription != null;
    }

    private class Characteristics {
        UUID uuid;
        String name;
        Characteristics(byte[] uuidBytes, byte[] nameBytes){
            try {
                uuid = BleUtils.asUuid(uuidBytes);
                name = new String(nameBytes);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
