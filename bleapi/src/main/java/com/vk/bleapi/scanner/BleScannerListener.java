package com.vk.bleapi.scanner;

import com.vk.bleapi.BleDevice;
import com.vk.bleapi.UserNearby;

import java.util.List;
import java.util.Map;

/**
 * Created by Voronov Viacheslav
 * on 5/9/2016
 * 5:30 PM
 */
public interface BleScannerListener {

    void onScanLaunched();
    void onScanStopped();
    void onScanUpdate(Map<String, BleDevice> deviceSet);
    void onScanUpdate(BleDevice device);
    void onUsersUpdate(List<UserNearby> users);
    void onDeviceLost(BleDevice device);
    void onScanFail(int code);
}
