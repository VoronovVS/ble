package com.vk.bleapi.scanner;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.media.MediaMetadataCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

import com.google.gson.Gson;
import com.vk.bleapi.BleDevice;
import com.vk.bleapi.advetiser.BleAdvertiserListener;
import com.vk.bleapi.BleUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 4/17/2016
 * 10:35 PM
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public abstract class BaseBleScanner implements BleScanner {

    protected static final String LOGS = "BBleScanner";

    protected static final long CLEANING_FREQUENCY = 9000;
    protected static final long SCAN_RESULT_LIFE_TIME = 22000;
    protected static final long SCAN_FREQUENCY = 8000;
    protected static final long CONNECTION_INIT_FREQUENCY = 3000;

    private static final String BLE_SERVICE_KEY = "B206EE5D-17EE-40C1-92BA-462A038A33D2"; //fixme : remove
    private static final UUID BLE_GENERATED_UUID_CHARACTERISTIC = UUID.fromString("E669893C-F4C2-4604-800A-5252CED237F9"); //fixme : remove

    private Context context;
    private BluetoothAdapter adapter;
    private BleScannerListener listener;
    private UUID serviceUuidFilter;

    private Handler handler = new Handler();

    private final Map<String, BleDevice> devices = new HashMap<>();

    private Map<String, ConnectingDevice> connectingDevices = new HashMap<>();
    private final Map<String, BluetoothGatt> currentGatts = new HashMap<>();

    private Thread connectionThread;
    private Thread serviceDiscoveryThread;

    private final Map<String, BluetoothDevice> discoveredDevices = new HashMap<>();
    private Queue<BluetoothDevice> connectionQueue = new LinkedList<>();
    private final Queue<BluetoothGatt> serviceDiscoveryQueue = new LinkedList<>();

    private boolean isConnecting;

    private class ConnectingDevice {

        long initTime = System.currentTimeMillis();
        String address;

    }

    public enum ScannerState {
        RUNNING,
        PAUSED,
        STOPPED,
    }

    private ScannerState scannerState = ScannerState.STOPPED;

    public BaseBleScanner(Context context) {
        this.context = context.getApplicationContext();
        this.adapter = BleUtils.getBluetoothAdapter(context);
    }

    @Override
    public void setListener(BleScannerListener listener) {
        this.listener = listener;
    }

    @Override
    public void setServiceUuidFilter(UUID uuid) {
        this.serviceUuidFilter = uuid;
    }

    @Override
    public void launch() {
        scannerState = ScannerState.RUNNING;
        connectionQueue.clear();
        launchPeriodicTasks();
    }

    @Override
    public void stop() {
        scannerState = ScannerState.STOPPED;
        devices.clear();

        stopPeriodicTasks();

        for(Map.Entry<String, BluetoothGatt> pair: currentGatts.entrySet()){
            pair.getValue().close();
        }
        currentGatts.clear();
        listener.onScanStopped();

        isConnecting = false;
    }

    @Override
    public void pause() {
        scannerState = ScannerState.PAUSED;
        stopPeriodicTasks();
        cancelCurrentScan();
        isConnecting = false;
    }

    protected abstract void cancelCurrentScan();

    @Override
    public void resume() {
        scannerState = ScannerState.RUNNING;
        launchPeriodicTasks();
    }

    @Override
    public Map<String, BleDevice> getCurrentDevices() {
        return devices;
    }

    protected BluetoothAdapter getAdapter() {
        return adapter;
    }

    @Override
    public boolean isRunning() {
        return scannerState == ScannerState.RUNNING;
    }

    protected abstract void restartScan();

    private void launchPeriodicTasks() {
        handler.postDelayed(cleaningTask, CLEANING_FREQUENCY);
        handler.postDelayed(periodicScanTask, SCAN_FREQUENCY);
        handler.postDelayed(periodicConnectTask, 1000);
    }

    private void stopPeriodicTasks(){
        handler.removeCallbacks(cleaningTask);
        handler.removeCallbacks(periodicScanTask);
        handler.removeCallbacks(periodicConnectTask);
    }

    Runnable cleaningTask = new Runnable() {
        @Override
        public void run() {
            removeOldScanResults();
            handler.postDelayed(this, SCAN_FREQUENCY);
        }
    };

    Runnable periodicScanTask = new Runnable() {
        @Override
        public void run() {
            restartScan();
            removeAllConnections();
            handler.postDelayed(this, SCAN_FREQUENCY);
        }
    };

    Runnable periodicConnectTask = new Runnable() {
        @Override
        public void run() {
            sendOnDeviceDiscoveredBroadcast();
            handler.postDelayed(this, CONNECTION_INIT_FREQUENCY);
        }
    };

    private void removeAllConnections(){
        synchronized (currentGatts){
            for(Map.Entry<String, BluetoothGatt> pair: currentGatts.entrySet()){
                if (pair.getValue() != null) {
                    pair.getValue().close();
                }
            }
            currentGatts.clear();
            discoveredDevices.clear();
            connectingDevices.clear();
            serviceDiscoveryQueue.clear();

            isConnecting = false;
        }
    }

    private synchronized void removeOldScanResults() {
        Iterator<Map.Entry<String, BleDevice>> iterator = devices.entrySet().iterator();

        long currentTime = System.currentTimeMillis();

        while (iterator.hasNext()) {
            Map.Entry<String, BleDevice> pair = iterator.next();
            BleDevice device = pair.getValue();
            boolean remove = false;
            synchronized (devices) {
                if (currentTime - device.getSynchronizationTime() > SCAN_RESULT_LIFE_TIME) {
                    iterator.remove();
                    remove = true;
                }
            }

            if (remove && listener != null) {
                listener.onDeviceLost(device);
            }
        }
    }

    protected BleScannerListener getListener(){
        return listener;
    }

    protected void initConnection(){
        if(connectionThread == null){
            connectionThread = new Thread(() -> {
                connectionLoop();
                connectionThread.interrupt();
                connectionThread = null;
            });

            connectionThread.start();
        }
    }

    private void connectionLoop(){
        while(!connectionQueue.isEmpty()){

            BluetoothDevice device = connectionQueue.poll();
            if (device == null){
                continue;
            }

            ConnectingDevice connDevice = new ConnectingDevice();
            connDevice.address = device.getAddress();
            connectingDevices.put(device.getAddress(), connDevice);

            currentGatts.put(device.getAddress(), device.connectGatt(context, true, gattCallback));
            Log.d(LOGS, "Connecting to device: " + device.getAddress());
            if (currentGatts.size() > 2){
                break;
            }

            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {}
        }
    }

    private void initServiceDiscovery(){
        if(serviceDiscoveryThread == null){
            serviceDiscoveryThread = new Thread(() -> {
                adapter.cancelDiscovery();
                serviceDiscovery();

                serviceDiscoveryThread.interrupt();
                serviceDiscoveryThread = null;
            });

            serviceDiscoveryThread.start();
        }
    }

    private void serviceDiscovery(){
        while(!serviceDiscoveryQueue.isEmpty()){
            BluetoothGatt gatt = serviceDiscoveryQueue.poll();
            gatt.discoverServices();
            Log.d(LOGS, ": " + gatt.getDevice().getAddress());
            try {
                Thread.sleep(250);
            } catch (InterruptedException e){}
        }
    }

    protected void foundDevice(ScanResult result) {

        BluetoothDevice device = result.getDevice();

        Log.d(LOGS, "On device found " + device.getAddress());

        BleDevice bleDevice = devices.get(device.getAddress());
        if (bleDevice == null){
            bleDevice = new BleDevice();
            bleDevice.setAddress(device.getAddress());

            if (result.getScanRecord() != null){
                if (checkDeviceServices(result) && !TextUtils.isEmpty(result.getScanRecord().getDeviceName())) {
                    bleDevice.setName(result.getScanRecord().getDeviceName());
                }
            }
            devices.put(device.getAddress(), bleDevice);
        }

        if (result.getScanRecord() != null) {
            SparseArray<byte[]> manufacturerSpecificData = result.getScanRecord().getManufacturerSpecificData();
            StringBuilder manufacturer = new StringBuilder();
            for (int i = 0; i < manufacturerSpecificData.size(); i++) {
                if (manufacturerSpecificData.get(i) != null) {
                    manufacturer.append(new String(manufacturerSpecificData.get(i))).append(" ");
                }
            }
            bleDevice.setManufacturer(manufacturer.toString());
        }

        bleDevice.setSynchronizationTime(System.currentTimeMillis());

        if (!discoveredDevices.containsKey(device.getAddress()) && (bleDevice.getUuid() == null || TextUtils.isEmpty(bleDevice.getName()))) {
            discoveredDevices.put(device.getAddress(), device);
            Log.d(LOGS, "Queuing device: " + device.getAddress());
        }

        if (listener != null){
            listener.onScanUpdate(bleDevice);
        }
    }

    private boolean checkDeviceServices(ScanResult result){
        if(result.getScanRecord() != null && result.getScanRecord().getServiceUuids() != null){
            for (ParcelUuid uuid: result.getScanRecord().getServiceUuids()){
                if (BLE_SERVICE_KEY.equals(uuid.getUuid().toString().toUpperCase())){
                    return true;
                }
            }
        }

        return false;
    }

    private void sendOnDeviceDiscoveredBroadcast(){
        if (discoveredDevices.size() == 0){
            return;
        }

/*        String[] addresses;
        synchronized (discoveredDevices) {
            addresses = new String[discoveredDevices.size()];
            int index = 0;

            for(Map.Entry<String, BluetoothDevice> pair: discoveredDevices.entrySet()){
                addresses[index] = pair.getKey();
                index++;
            }
        }*/

        List<BleDevice> devices;
        synchronized (discoveredDevices) {

            devices = new ArrayList<>();
            for (Map.Entry<String, BleDevice> pair : this.devices.entrySet()) {
                BleDevice device = pair.getValue();
                if (device.getUuid() == null){
                    devices.add(device);
                }
            }

        }
        Intent intent = new Intent(ACTION_ON_NEW_DEVICES_DISCOVERED);
        String json = new Gson().toJson(devices);        //// FIXME: 5/19/2016 remove GSON
        intent.putExtra(EXTRA_DEVICE_ADDRESSES, json);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private void sendOnInfoReadBroadcast(BluetoothDevice device, UUID uuid){
        BluetoothDevice connectingDevice = discoveredDevices.get(device.getAddress());
        if (connectingDevice != null){
            discoveredDevices.remove(device.getAddress());
        }

        Intent intent = new Intent(ACTION_ON_CHARACTERISTIC_READ_SUCCESS);
        intent.putExtra(EXTRA_DEVICE_ADDRESSES, device.getAddress());
        intent.putExtra(EXTRA_DEVICE_UUID, uuid);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void connectToDevices(List<String> devices) {
        for (String address: devices){
            connectionQueue.add(discoveredDevices.get(address));
        }
        if (!isConnecting){
            isConnecting = true;
            initConnection();
        }
    }

    @Override
    public void onDeviceInfoReceived(String address, String userId, UUID uuid) {
        synchronized (discoveredDevices) {

            for (Map.Entry<String, BleDevice> pair: devices.entrySet()){
                BleDevice bleDevice = pair.getValue();
                if ((bleDevice.getAddress() != null && bleDevice.getAddress().equals(address)) || ((bleDevice.getName() != null && bleDevice.getName().equals(userId)))){
                    bleDevice.setUuid(uuid);
                    bleDevice.setName(userId);

                    BluetoothDevice device = discoveredDevices.get(address);
                    connectionQueue.remove(device);
                    discoveredDevices.remove(address);
                    BluetoothGatt gatt = currentGatts.get(address);
                    if (gatt != null) {
                        gatt.disconnect();
                        gatt.close();
                        currentGatts.remove(address);
                    }

                    if (listener != null) {
                        listener.onScanUpdate(bleDevice);
                    }
                }
            }
        }
    }

    private BluetoothGattCallback gattCallback = new BluetoothGattCallback() {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                currentGatts.remove(gatt.getDevice().getAddress());
                gatt.close();
                return;
            }

            if (status == 133) {
                Log.e(LOGS, "Got the status 133 bug, closing gatt");
                connectingDevices.remove(gatt.getDevice().getAddress());
                currentGatts.remove(gatt.getDevice().getAddress());
                gatt.close();
                return;
            } else if (status == BluetoothGatt.GATT_SUCCESS){
                synchronized (serviceDiscoveryQueue) {
                    if (!serviceDiscoveryQueue.contains(gatt)) {
                        serviceDiscoveryQueue.add(gatt);
                    }

                    if (serviceDiscoveryQueue.size() == currentGatts.size()){
                        initServiceDiscovery();
                    }
                }
                Log.d(LOGS, "BluetoothGatt.GATT_SUCCESS " + gatt.getDevice().getAddress());
            } else {
                Log.d(LOGS, "onConnectionStateChange " + getGattErrorDescription(status) + " " + gatt.getDevice().getAddress());
                currentGatts.remove(gatt.getDevice().getAddress());
                gatt.disconnect();
                gatt.close();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);

            switch (status){
                case BluetoothGatt.GATT_SUCCESS:
                    Log.d(LOGS, "onServicesDiscovered BluetoothGatt.GATT_SUCCESS " + gatt.getDevice().getAddress());
                    break;
                default:
                    Log.d(LOGS, "onServicesDiscovered " + getGattErrorDescription(status) + " " + gatt.getDevice().getAddress());
                    currentGatts.remove(gatt.getDevice().getAddress());
                    gatt.disconnect();
                    gatt.close();
                    return;
            }

            Log.d(LOGS, "On services discovered: " + gatt.getDevice().getAddress() + " Time: " + (System.currentTimeMillis() - connectingDevices.get(gatt.getDevice().getAddress()).initTime));

            BluetoothGattService service = gatt.getService(serviceUuidFilter);
            if (service == null){
                currentGatts.remove(gatt.getDevice().getAddress());
                gatt.disconnect();
                gatt.close();
                return;
            }

            Log.d(LOGS, "On initiated reading: " + gatt.getDevice().getAddress() + " Time: " + (System.currentTimeMillis() - connectingDevices.get(gatt.getDevice().getAddress()).initTime));
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(BLE_GENERATED_UUID_CHARACTERISTIC);
            gatt.readCharacteristic(characteristic);

        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);

            BleDevice device = devices.get(gatt.getDevice().getAddress());
            if (device == null) {
                return;
            }

            if (BLE_GENERATED_UUID_CHARACTERISTIC.equals(characteristic.getUuid())) {
                try {
                    byte[] uuidBytes = characteristic.getValue();
                    if (uuidBytes != null) {
                        device.setUuid(BleUtils.asUuid(uuidBytes));
                        Log.d(LOGS, "uuid=" + device.getUuid().toString());
                        sendOnInfoReadBroadcast(gatt.getDevice(), device.getUuid());
                        currentGatts.remove(gatt.getDevice().getAddress());
                        gatt.disconnect();
                        gatt.close();
                    }
                    Log.d(LOGS, "On uuid read success: " + gatt.getDevice().getAddress() + " Time: " + (System.currentTimeMillis() - connectingDevices.get(gatt.getDevice().getAddress()).initTime));

                } catch (Exception e) {
                    e.printStackTrace();
                    gatt.disconnect();
                    gatt.close();
                    currentGatts.remove(gatt.getDevice().getAddress());
                }
            }

            device.setSynchronizationTime(System.currentTimeMillis());

            if(currentGatts.size() == 0){
                isConnecting = false;
            }

            if (listener != null) {
                listener.onScanUpdate(device);
            }
        }
    };

    private String getGattErrorDescription(int status){
        switch (status) {
            case BluetoothGatt.GATT_FAILURE:
                return "GATT_FAILURE";
            case BluetoothGatt.GATT_CONNECTION_CONGESTED:
                return "GATT_READ_NOT_PERMITTED";
            case BluetoothGatt.GATT_READ_NOT_PERMITTED:
                return "GATT_READ_NOT_PERMITTED";
            case BluetoothGatt.GATT_WRITE_NOT_PERMITTED:
                return "GATT_WRITE_NOT_PERMITTED";
            case BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION:
                return "GATT_INSUFFICIENT_AUTHENTICATION";
            case BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED:
                return "GATT_REQUEST_NOT_SUPPORTED";
            case BluetoothGatt.GATT_INSUFFICIENT_ENCRYPTION:
                return "GATT_INSUFFICIENT_ENCRYPTION";
            case BluetoothGatt.GATT_INVALID_OFFSET:
                return "GATT_INVALID_OFFSET";
            case BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH:
                return "GATT_INVALID_ATTRIBUTE_LENGTH";
            default:
                return "GATT_UNKNOWN_ERROR";
        }
    }
}
