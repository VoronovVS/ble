package com.vk.bleapi.scanner;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.vk.bleapi.BleDevice;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 4/26/2016
 * 10:52 PM
 */
public class DeviceCache {

    private static final Map<String, CacheResult> devicesCache = new HashMap<>();

    private DeviceCache(){}

    public static DeviceCache instance(){
        return new DeviceCache();
    }

    public void update(BleDevice device){
        synchronized (devicesCache) {
            CacheResult cache = devicesCache.get(device.getAddress());
            if (cache == null){
                cache = new CacheResult();
                cache.setAddress(device.getAddress());
                devicesCache.put(device.getAddress(), cache);
            }

            cache.setLastSynchTime(System.currentTimeMillis());
            cache.setName(device.getName());
            cache.setUuid(device.getUuid());
        }
    }

    public static boolean drop(){
        boolean notEmpty = devicesCache.size() > 0;
        devicesCache.clear();
        return notEmpty;
    }

    @Nullable
    public CacheResult getCacheResult(String address){
        return devicesCache.get(address);
    }

    public class CacheResult {

        private final static int UPDATE_TIME = 10*60*1000; //10 minutes

        private UUID uuid;
        private String name;
        private String address;

        private long lastSynchTime;
        private long lastUuidSynchTime;
        private long lastNameSynchTime;

        private void setName(String name){
            if (TextUtils.isEmpty(name)){
                return;
            }

            this.name = name;
            lastNameSynchTime = System.currentTimeMillis();
        }

        private void setUuid(UUID uuid){
            if (uuid == null){
                return;
            }

            this.uuid = uuid;
            lastUuidSynchTime = System.currentTimeMillis();
        }

        private void setLastSynchTime(long synchTime){
            this.lastSynchTime = synchTime;
        }

        public UUID getUuid() {
            return uuid;
        }

        public String getName() {
            return name;
        }

        public long getSynchTime() {
            return lastSynchTime;
        }

        public boolean requiresCharacteristicsUpdate(){
            return/* System.currentTimeMillis() - lastNameSynchTime > UPDATE_TIME ||*/ System.currentTimeMillis() - lastUuidSynchTime > UPDATE_TIME;
        }

        public String getAddress() {
            return address;
        }

        private void setAddress(String address) {
            this.address = address;
        }
    }

}
