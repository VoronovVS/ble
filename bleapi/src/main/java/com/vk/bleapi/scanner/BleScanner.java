package com.vk.bleapi.scanner;

import com.vk.bleapi.BleDevice;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 4/17/2016
 * 10:16 PM
 */
public interface BleScanner {

    String ACTION_ON_NEW_DEVICES_DISCOVERED = "new_device_discovered";
    String ACTION_ON_CHARACTERISTIC_READ_SUCCESS = "on_characteristic_read_success";

    String EXTRA_DEVICE_ADDRESSES = "device_addresses";
    String EXTRA_DEVICE_UUID = "device_uuid";

    void launch();
    void stop();
    void pause();
    void resume();

    void setListener(BleScannerListener listener);
    void setServiceUuidFilter(UUID uuid);
    void connectToDevices(List<String> devices);
    void onDeviceInfoReceived(String address, String userId, UUID uuid);
    Map<String, BleDevice> getCurrentDevices();

    boolean isRunning();

}
