package com.vk.ble.di.service;

import com.vk.ble.network.HttpAdapter;
import com.vk.ble.network.HttpService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Voronov Viacheslav
 * on 2/4/2016
 * 3:16 PM
 */
@Module
public class ServiceModule {

    @ServiceScope
    @Provides
    public HttpService providesApiService(){
        return HttpAdapter.getAdapter();
    }

}
