package com.vk.ble.di.service;

import com.vk.ble.service.BleTaskService;

import dagger.Subcomponent;

/**
 * Created by Voronov Viacheslav
 * on 2/4/2016
 * 3:15 PM
 */

@ServiceScope
@Subcomponent(modules=ServiceModule.class)
public interface ServiceComponent {

    void inject(BleTaskService bleTaskService);

}
