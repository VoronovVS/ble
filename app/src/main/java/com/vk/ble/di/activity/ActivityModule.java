package com.vk.ble.di.activity;

import android.app.Activity;

import com.vk.ble.network.HttpAdapter;
import com.vk.ble.network.HttpService;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Voronov Viacheslav
 * on 1/9/2016
 * 8:46 PM
 */

@Module
public class ActivityModule {

    final private Activity activity;

    public ActivityModule(Activity activity){
        this.activity = activity;
    }

    @ActivityScope
    @Provides
    public HttpService providesApiService(){
        return HttpAdapter.getAdapter();
    }

}
