package com.vk.ble.di.app;

import com.vk.ble.App;
import com.vk.ble.di.activity.ActivityComponent;
import com.vk.ble.di.activity.ActivityModule;
import com.vk.ble.di.service.ServiceComponent;
import com.vk.ble.di.service.ServiceModule;

import dagger.Component;

/**
 * Created by Voronov Viacheslav
 * on 1/9/2016
 * 8:44 PM
 */
@ApplicationScope
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(App app);

    ActivityComponent component(ActivityModule activityModule);
    ServiceComponent component(ServiceModule module);

}
