package com.vk.ble.di.service;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Voronov Viacheslav
 * on 2/4/2016
 * 3:15 PM
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ServiceScope {
}
