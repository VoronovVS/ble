package com.vk.ble.di.activity;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Voronov Viacheslav
 * on 1/9/2016
 * 8:48 PM
 */
@Scope
@Retention(RetentionPolicy.RUNTIME) public @interface ActivityScope{
}
