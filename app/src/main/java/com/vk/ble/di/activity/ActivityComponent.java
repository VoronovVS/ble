package com.vk.ble.di.activity;

import com.vk.ble.MainActivity;
import com.vk.ble.di.fragment.FragmentComponent;
import com.vk.ble.di.fragment.FragmentModule;

import dagger.Subcomponent;

/**
 * Created by Voronov Viacheslav
 * on 1/9/2016
 * 8:46 PM
 */

@ActivityScope
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

    FragmentComponent component(FragmentModule module);
}
