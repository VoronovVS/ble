package com.vk.ble.di.app;

import android.app.Application;

import com.polidea.rxandroidble.RxBleClient;
import com.polidea.rxandroidble.internal.RxBleLog;
import com.vk.ble.App;
import com.vk.ble.service.BleManager;
import com.vk.bleapi.advetiser.BleAdvertiser;
import com.vk.bleapi.scanner.BleScanner;
import com.vk.bleapi.scanner.MiniScanner;
import com.vk.bleapi.scanner.ModernBleScanner;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Voronov Viacheslav
 * on 5/9/2016
 * 3:50 PM
 */
@Module
public class ApplicationModule {

    final Application application;

    public ApplicationModule(Application application){
        this.application = application;
    }

    @Provides
    @ApplicationScope
    public App providesApp(){
        return (App)application;
    }

    @Provides
    @ApplicationScope
    public BleManager providesBleManager(BleScanner scanner, BleAdvertiser advertiser){
        return new BleManager(application.getApplicationContext(), scanner, advertiser);
    }

    @Provides
    @ApplicationScope
    public BleScanner providesBleScanner(){
        return new MiniScanner(application);
    }

    @Provides
    @ApplicationScope
    public BleAdvertiser providesBleAdvertiser(){
        return new BleAdvertiser(application);
    }

    @Provides
    @ApplicationScope
    public RxBleClient providesRxBleClient(){
        RxBleClient.setLogLevel(RxBleLog.DEBUG);
        return RxBleClient.create(application);
    }
}
