package com.vk.ble.di.fragment;

import com.vk.ble.fragment.ScannerFragment;
import com.vk.ble.fragment.StartFragment;

import dagger.Subcomponent;

/**
 * Created by Voronov Viacheslav
 * on 1/23/2016
 * 7:28 PM
 */

@FragmentScope
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {

    void inject(StartFragment fragment);
    void inject(ScannerFragment fragment);

}
