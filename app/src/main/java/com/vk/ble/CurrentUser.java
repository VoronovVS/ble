package com.vk.ble;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.vk.ble.model.User;

/**
 * Created by Voronov Viacheslav
 * on 5/3/2016
 * 11:06 AM
 */
public class CurrentUser {

    public static final String BROADCAST_ACTION_USER_UPDATE = "user_updated";

    private static final String PREF_FILE_NAME = "current_user_prefs";
    private static final String CURRENT_USER = "current_user";

    private static User currentUser;

    public static void setCurrentUser(Context context, User user) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        String userJson = new Gson().toJson(user);
        editor.putString(CURRENT_USER, userJson);
        editor.apply();

        currentUser = user;
        sendCurrentUserUpdateBroadcast(context);
    }

    public static User getCurrentUser(Context context){
        if (currentUser != null){
            return currentUser;
        }

        SharedPreferences preferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        if (preferences.contains(CURRENT_USER)){
            try {
                String json = preferences.getString(CURRENT_USER, null);
                return new Gson().fromJson(json, User.class);
            } catch (JsonSyntaxException e){
                e.printStackTrace();
            }
        }

        return null;
    }

    private static void sendCurrentUserUpdateBroadcast(Context context){
        Intent intent = new Intent(BROADCAST_ACTION_USER_UPDATE);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

}
