package com.vk.ble.model.repository;

import com.vk.ble.model.User;

/**
 * Created by Voronov Viacheslav
 * on 5/3/2016
 * 10:46 AM
 */
public class UsersRepository {

    private static User[] users = new User[]{
            new User("test user 1", "user1@m.com", "11", "B320EE30-8726-4DE9-82F6-9337796247FE", "547:f895efba2287dba409282672e27e05d5de4c", 547 ),
            new User("test user 2", "user2@m.com", "11", "4B3E95EF-12E9-4CFF-8A7D-0EE77512D872", "548:5ec77e282022ae679ec7bfe6a622455fb093", 548 ),
            new User("test user 3", "user3@m.com", "11", "4CFE1B80-BA5C-4356-B823-726AF3C36CE1", "549:f95574ba9ff7f06c57fcb368694be523fc1f", 549 ),
            new User("test user 4", "user4@m.com", "11", "1CFE1B80-BA5C-4356-B823-726AF3C36CE1", "550:fea46dc92b51e887a89ef4b3a09de9c9b27d", 550 ),
            new User("test user 5", "user5@m.com", "11", "E053119D-C282-476C-9F73-B8CC9E6BD754", "551:08ef5daeda7cf7e4825ba4c3c0c8005e5901", 551 )
    };

    public static User[] getUsers(){
        return users;
    }

    public static User getUser(int position){
        return users[position];
    }

}
