package com.vk.ble.model;

import com.google.gson.annotations.SerializedName;
import com.vk.bleapi.UserNearby;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Voronov Viacheslav
 * on 5/4/2016
 * 11:17 PM
 */
public class UsersNearbyWrapper implements Serializable {

    @SerializedName("method_name")
    private String methodName;

    private List<UserNearby> data;

    private String status;

    private String message;


    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public List<UserNearby> getData() {
        return data;
    }

    public void setData(List<UserNearby> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
