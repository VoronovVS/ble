package com.vk.ble.model;

import com.vk.bleapi.BleDevice;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Voronov Viacheslav
 * on 4/26/2016
 * 12:46 AM
 */
public class AdvancedBleDevice extends BleDevice {

    private AtomicLong uuidCharacteristicReadTime;
    private AtomicLong nameCharacteristicReadTime;
    private long creationTime = System.currentTimeMillis();

    public AtomicLong getNameCharacteristicReadTime() {
        return nameCharacteristicReadTime;
    }

    public void setNameCharacteristicReadTime(long nameCharacteristicReadTime) {
        this.nameCharacteristicReadTime = new AtomicLong(nameCharacteristicReadTime);
    }

    public AtomicLong getUuidCharacteristicReadTime() {
        return uuidCharacteristicReadTime;
    }

    public void setUuidCharacteristicReadTime(long uuidCharacteristicReadTime) {
        this.uuidCharacteristicReadTime = new AtomicLong(uuidCharacteristicReadTime);
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }
}
