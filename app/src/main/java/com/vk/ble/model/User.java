package com.vk.ble.model;

import java.io.Serializable;

/**
 * Created by Voronov Viacheslav
 * on 5/3/2016
 * 10:44 AM
 */
public class User implements Serializable {

    private String userName;
    private String email;
    private String password;
    private String deviceId;
    private String token;
    private int userId;

    public User(String userName, String email, String password, String deviceId, String token, int userId){
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.deviceId = deviceId;
        this.token = token;
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getToken() {
        return token;
    }

    public int getUserId() {
        return userId;
    }
}
