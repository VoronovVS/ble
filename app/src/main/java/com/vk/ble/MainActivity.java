package com.vk.ble;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.vk.ble.di.activity.ActivityComponent;
import com.vk.ble.fragment.ScannerFragment;
import com.vk.ble.fragment.StartFragment;
import com.vk.ble.model.User;
import com.vk.ble.model.repository.UsersRepository;
import com.vk.bleapi.BleUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_BLUETOOTH = 131;

    private ActivityComponent activityComponent;

    @Bind(R.id.bluetoothNotEnabledLayout)
    View bluetoothNotEnabledLayout;
    @Bind(R.id.container)
    View contentLayout;
    @Bind(R.id.bluetoothNotSupported)
    View bluetoothNotSupportedLayout;

    private boolean bluetoothRequested;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        injectDependencies();

        ButterKnife.bind(this);

        User currentUser = CurrentUser.getCurrentUser(this);
        if (currentUser == null){
            currentUser = UsersRepository.getUser(0);
            CurrentUser.setCurrentUser(this, currentUser);
        }

        if (savedInstanceState == null){
            getSupportFragmentManager().beginTransaction().add(R.id.container, new StartFragment(), "StartTag").commit();
        }
    }

    protected void injectDependencies() {
        if (activityComponent == null) {
            activityComponent = ((App) getApplicationContext()).createActivityComponent(this);
        }
        activityComponent.inject(this);
    }

    public ActivityComponent getActivityComponent(){
        return activityComponent;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!BleUtils.isBleSupported(this)){
            switchUiToBleNotSupported();
            return;
        }

        if (!BleUtils.getBluetoothAdapter(this).isEnabled()){
            onBluetoothDisabled(false);
            switchUiToBluetoothNotEnabled();
        }

        switchUiToContent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        User[] users = UsersRepository.getUsers();
        for (int i = 0; i < users.length; i++){

            User user = users[i];
            MenuItem item = menu.add(1, i, i, user.getUserName());
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getGroupId() == 1){
            CurrentUser.setCurrentUser(this, UsersRepository.getUser(item.getItemId()));

        }
        return super.onOptionsItemSelected(item);
    }

    private void switchUiToBleNotSupported(){
        bluetoothNotEnabledLayout.setVisibility(View.GONE);
        bluetoothNotSupportedLayout.setVisibility(View.VISIBLE);
    }

    private void switchUiToContent(){
        bluetoothNotEnabledLayout.setVisibility(View.GONE);
        bluetoothNotSupportedLayout.setVisibility(View.GONE);
    }

    private void switchUiToBluetoothNotEnabled(){
        bluetoothNotEnabledLayout.setVisibility(View.GONE);
        bluetoothNotSupportedLayout.setVisibility(View.VISIBLE);
    }

    public void onBluetoothDisabled(boolean forcedRequest) {
        if (forcedRequest || !bluetoothRequested) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_CODE_BLUETOOTH);

            bluetoothRequested = true;
        }
    }

    @OnClick(R.id.enableBluetooth)
    void onEnableBluetoothClick(){
        onBluetoothDisabled(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_CANCELED) {
                //Bluetooth not enabled.
                Toast.makeText(this, "Bluetooth not enabled", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void displayBleList(){
        ScannerFragment fragment = new ScannerFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, fragment, "ScannerTag").addToBackStack("Scanner").commit();
    }

    public void hideBleList(){
        getSupportFragmentManager().popBackStack();
    }
}
