package com.vk.ble.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vk.ble.R;
import com.vk.ble.model.AdvancedBleDevice;
import com.vk.bleapi.BleDevice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Voronov Viacheslav
 * on 4/18/2016
 * 12:43 AM
 */
public class BleDevicesAdapter extends BaseAdapter {

    private final List<AdvancedBleDevice> items = new ArrayList<>();

    @Override
    public int getCount() {
        return items.size();
    }

    public void removeItem(BleDevice device){
        synchronized (items) {
            if (items.contains(device)) {
                items.remove(device);
                notifyDataSetChanged();
            }
        }
    }

    @Override
    public AdvancedBleDevice getItem(int position) {
        return items.get(position);
    }

    public void setItems(Map<String, BleDevice> items){
        this.items.clear();
        for (Map.Entry<String, BleDevice> entry : items.entrySet()){
            updateItem(entry.getValue());
        }

        notifyDataSetChanged();
    }

    public void updateItem(BleDevice item){
        synchronized (items) {
            if (items.contains(item)) {
                updateBleDeviceValues(items.get(items.indexOf(item)), item);
            } else {
                AdvancedBleDevice newDevice = new AdvancedBleDevice();
                newDevice.setCreationTime(System.currentTimeMillis());
                updateBleDeviceValues(newDevice, item);
                items.add(newDevice);
            }
        }

        synchronized (items) {
            removeDuplicates();
        }

        notifyDataSetChanged();
    }

    private void removeDuplicates(){
        List<String> uuids = new ArrayList<>();
        Set<String> duplicates = new HashSet<>();
        for (AdvancedBleDevice device: items){
            if (device.getUuid() == null){
                continue;
            }
            String uuid = device.getUuid().toString();
            if (uuids.contains(uuid)){
                duplicates.add(uuid);
            } else {
                uuids.add(uuid);
            }
        }

        if (duplicates.size() == 0){
            return;
        }

        for (String uuid : duplicates){
            List<AdvancedBleDevice> devices = getDevicesByUuid(uuid);
            AdvancedBleDevice mostRecentDevice = null;
            for (AdvancedBleDevice device: devices){
                if (mostRecentDevice == null || mostRecentDevice.getSynchronizationTime() < device.getSynchronizationTime()){
                    mostRecentDevice = device;
                }
            }

            devices.remove(mostRecentDevice);

            for (AdvancedBleDevice device: devices){
                items.remove(device);
            }
        }
    }

    private List<AdvancedBleDevice> getDevicesByUuid(String uuid){
        List<AdvancedBleDevice> devices = new ArrayList<>();
        if (TextUtils.isEmpty(uuid)){
            return devices;
        }
        for (AdvancedBleDevice device: items){
            if (device.getUuid() != null && uuid.equals(device.getUuid().toString())){
                devices.add(device);
            }
        }

        return devices;
    }

    private void updateBleDeviceValues(AdvancedBleDevice item, BleDevice updated){
        if (item.getUuid() == null && updated.getUuid() != null){
            if (item.getUuidCharacteristicReadTime() == null){
                item.setUuidCharacteristicReadTime(System.currentTimeMillis() - item.getCreationTime());
            }
        }

        if (item.getName() == null && TextUtils.isEmpty(item.getName()) && updated.getName() != null) {
            if (item.getNameCharacteristicReadTime() == null) {
                item.setNameCharacteristicReadTime(System.currentTimeMillis() - item.getCreationTime());
            }
        }

        item.setManufacturer(updated.getManufacturer());

        if (updated.getUuid() != null){
            item.setUuid(updated.getUuid());
        }

        if (!TextUtils.isEmpty(updated.getName())){
            item.setName(updated.getName());
        }

        item.setSynchronizationTime(updated.getSynchronizationTime());
        item.setAddress(updated.getAddress());
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_device_info, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        AdvancedBleDevice bleDevice = getItem(position);

        viewHolder.name.setText(bleDevice.getName());
        viewHolder.address.setText(bleDevice.getAddress());

        viewHolder.creationTime.setText(String.valueOf(System.currentTimeMillis() - bleDevice.getCreationTime()));

        if (bleDevice.getUuid() != null) {
            viewHolder.uuid.setText(bleDevice.getUuid().toString().toUpperCase());
        }

        return convertView;
    }

    class ViewHolder {

        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.uuid)
        TextView uuid;
        @Bind(R.id.address)
        TextView address;
        @Bind(R.id.nameSynchTime)
        TextView nameSynchTime;
        @Bind(R.id.uuidSynchTime)
        TextView uuidSynchTime;
        @Bind(R.id.creationTime)
        TextView creationTime;

        ViewHolder(View view){
            ButterKnife.bind(this, view);
        }

    }
}
