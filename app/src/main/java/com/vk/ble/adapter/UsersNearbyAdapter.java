package com.vk.ble.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vk.ble.R;
import com.vk.bleapi.UserNearby;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Voronov Viacheslav
 * on 4/18/2016
 * 12:43 AM
 */
public class UsersNearbyAdapter extends BaseAdapter {

    private final List<UserNearby> items = new ArrayList<>();

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public UserNearby getItem(int position) {
        return items.get(position);
    }

    public void setItems(List<UserNearby> items){
        this.items.clear();
        if (items != null) {
            this.items.addAll(items);
        }

        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_device_info, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        UserNearby user = getItem(position);

        viewHolder.name.setText(user.getId());
        viewHolder.uuid.setText(user.getFullName());

        return convertView;
    }

    class ViewHolder {

        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.uuid)
        TextView uuid;
        @Bind(R.id.address)
        TextView address;
        @Bind(R.id.nameSynchTime)
        TextView nameSynchTime;
        @Bind(R.id.uuidSynchTime)
        TextView uuidSynchTime;
        @Bind(R.id.creationTime)
        TextView creationTime;

        ViewHolder(View view){
            ButterKnife.bind(this, view);
        }

    }
}
