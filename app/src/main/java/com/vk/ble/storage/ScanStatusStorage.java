package com.vk.ble.storage;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Voronov Viacheslav
 * on 5/9/2016
 * 11:27 PM
 */
public class ScanStatusStorage {

    private static final String PREF_FILE_NAME = "com.vk.ble.storage.ScanStatusStorage.PREF_FILE_NAME";
    private static final String KEY_SCAN_LAUNCHED = "com.vk.ble.storage.ScanStatusStorage.KEY_SCAN_LAUNCHED";

    public static void setScanStatus(Context context, boolean isRunning) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean(KEY_SCAN_LAUNCHED, isRunning);
        editor.apply();
    }

    public static boolean getScanLaunchStatus(Context context){
        return getPreferences(context).getBoolean(KEY_SCAN_LAUNCHED, false);
    }

    private static SharedPreferences getPreferences(Context context){
        return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }
}
