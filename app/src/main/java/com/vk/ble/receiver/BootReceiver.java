package com.vk.ble.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vk.ble.service.BleTaskService;

/**
 * Created by Voronov Viacheslav
 * on 5/9/2016
 * 7:57 PM
 */
public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            BleTaskService.initializeService(context);
        }
    }
}
