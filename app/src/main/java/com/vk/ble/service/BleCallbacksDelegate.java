package com.vk.ble.service;

import com.vk.bleapi.UserNearby;
import com.vk.bleapi.advetiser.BleAdvertiserListener;
import com.vk.bleapi.BleDevice;
import com.vk.bleapi.scanner.BleScannerListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Voronov Viacheslav
 * on 4/21/2016
 * 1:32 AM
 */
public class BleCallbacksDelegate implements BleAdvertiserListener, BleScannerListener {

    private BleManager bleManager;

    BleCallbacksDelegate(BleManager bleManager){
        this.bleManager = bleManager;
    }

    @Override
    public void onAdvertiseSuccess() {
        for (BleAdvertiserListener listener : getAdvertiserListeners()){
            listener.onAdvertiseSuccess();
        }
    }

    @Override
    public void onScanLaunched() {
        for (BleScannerListener listener : getScannerListeners()){
            listener.onScanLaunched();
        }
    }

    @Override
    public void onScanStopped() {
        for (BleScannerListener listener : getScannerListeners()){
            listener.onScanStopped();
        }
    }

    @Override
    public void onScanUpdate(Map<String, BleDevice> deviceSet) {
        for (BleScannerListener listener : getScannerListeners()){
            listener.onScanUpdate(deviceSet);
        }
    }

    @Override
    public void onScanUpdate(BleDevice device) {
        for (BleScannerListener listener : getScannerListeners()){
            listener.onScanUpdate(device);
        }
    }

    @Override
    public void onUsersUpdate(List<UserNearby> users) {
        for (BleScannerListener listener : getScannerListeners()){
            listener.onUsersUpdate(users);
        }
    }

    @Override
    public void onDeviceLost(BleDevice device) {
        for (BleScannerListener listener : getScannerListeners()){
            listener.onDeviceLost(device);
        }
    }

    @Override
    public void onAdvertiseFail(int reason) {
        for (BleAdvertiserListener listener : getAdvertiserListeners()){
            listener.onAdvertiseFail(reason);
        }
    }

    @Override
    public void onScanFail(int code) {
        for (BleScannerListener listener : getScannerListeners()){
            listener.onScanFail(code);
        }
    }

    private List<BleAdvertiserListener> getAdvertiserListeners(){
        return bleManager == null ? new ArrayList<>() : bleManager.getAdvertiserListeners();
    }

    private List<BleScannerListener> getScannerListeners(){
        return bleManager == null ? new ArrayList<>() : bleManager.getScannerListeners();
    }
}
