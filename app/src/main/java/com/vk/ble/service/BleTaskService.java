package com.vk.ble.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.ScanCallback;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.vk.ble.App;
import com.vk.ble.MainActivity;
import com.vk.ble.R;
import com.vk.ble.bluetooth.GattListener;
import com.vk.ble.bluetooth.GattReceiver;
import com.vk.ble.di.service.ServiceComponent;
import com.vk.bleapi.UserNearby;
import com.vk.ble.model.UsersNearbyWrapper;
import com.vk.ble.network.ErrorsSafeSubscriber;
import com.vk.ble.network.HttpAdapter;
import com.vk.ble.storage.ScanStatusStorage;
import com.vk.bleapi.BleDevice;
import com.vk.bleapi.BleUtils;
import com.vk.bleapi.advetiser.BleAdvertiserListener;
import com.vk.bleapi.scanner.BleScanner;
import com.vk.bleapi.scanner.BleScannerListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Voronov Viacheslav
 * on 4/21/2016
 * 12:13 AM
 */
public class BleTaskService extends Service implements BleAdvertiserListener, BleScannerListener, GattListener {

    private static final int FOREGROUND_SERVICE_NOTIFICATION_ID = 1000;
    private static final int SCAN_FAILED_NOTIFICATION_ID = 1001;
    private static final int PING_FREQUENCY = 8000;

    public static String SERVICE_START = "ServiceStart";
    public static String SERVICE_INITIALIZE = "ServiceInitialize";
    public static String SERVICE_STOP = "ServiceStop";

    private static final IntentFilter BLUETOOTH_INTENT_FILTER = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
    private static final IntentFilter GATT_INTENT_FILTER = new IntentFilter();
    static {
        GATT_INTENT_FILTER.addAction(BleScanner.ACTION_ON_NEW_DEVICES_DISCOVERED);
        GATT_INTENT_FILTER.addAction(BleScanner.ACTION_ON_CHARACTERISTIC_READ_SUCCESS);
    }

    private IBinder binder = new BleTaskServiceBinder();

    private static BleTaskService instance;

    @Inject
    BleManager bleManager;

    private NotificationManager mNotificationManager;

    private NotificationCompat.Builder mNotificationBuilder;
    private GattReceiver gattReceiver;
    private BluetoothReceiver bluetoothReceiver = new BluetoothReceiver();

    private final Set<BleDevice> synchronizingDevices = new HashSet<>();
    private boolean synchronizing = false;
    private boolean forceDisableBluetooth;

    private Handler handler = new Handler(Looper.getMainLooper());

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        ServiceComponent component = ((App)getApplicationContext()).createServiceComponent(this);
        component.inject(this);

        bleManager.addAdvertiserListener(this);
        bleManager.addScannerListener(this);

        gattReceiver = new GattReceiver(this);
        mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        LocalBroadcastManager.getInstance(this).registerReceiver(gattReceiver, GATT_INTENT_FILTER);
        registerReceiver(bluetoothReceiver, BLUETOOTH_INTENT_FILTER);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(gattReceiver);
        unregisterReceiver(bluetoothReceiver);

        onBleStop();

        bleManager.removeScannerListener(this);
        bleManager.removeAdvertiserListener(this);

    }

    public static void initializeService(Context context){
        Intent serviceIntent = new Intent(context, BleTaskService.class);
        serviceIntent.setAction(BleTaskService.SERVICE_INITIALIZE);
        context.startService(serviceIntent);
    }

    public static void launchService(Context context){
        ScanStatusStorage.setScanStatus(context, true);
        Intent serviceIntent = new Intent(context, BleTaskService.class);
        serviceIntent.setAction(BleTaskService.SERVICE_START);
        context.startService(serviceIntent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!BleUtils.enabled(this) || intent == null || intent.getAction() == null){
            return START_STICKY;
        }

        if (SERVICE_INITIALIZE.equals(intent.getAction())){
            if (ScanStatusStorage.getScanLaunchStatus(this) && BleUtils.enabled(this)) {
                bleManager.launchBle();
            }
        } else if (SERVICE_START.equals(intent.getAction())){
            bleManager.launchBle();
        } else if (SERVICE_STOP.equals(intent.getAction())){
            bleManager.getAdvertiser().stop();
            bleManager.getScanner().stop();

            ScanStatusStorage.setScanStatus(this, false);

            return START_NOT_STICKY;
        }
        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onAdvertiseSuccess() {

    }

    @Override
    public void onAdvertiseFail(int reason) {

    }

    @Override
    public void onScanLaunched() {
        displayOngoingNotification();
        handler.post(pingTask);
    }

    @Override
    public void onScanUpdate(Map<String, BleDevice> deviceSet) {
        onScanResults();
    }

    @Override
    public void onScanUpdate(BleDevice device) {
        onScanResults();
    }

    @Override
    public void onUsersUpdate(List<UserNearby> users) {

    }

    private void onScanResults(){
        handler.post(deviceListUpdateTask);
    }

    private Runnable deviceListUpdateTask = new Runnable() {
        @Override
        public void run() {
            mNotificationBuilder.setContentText(getString(R.string.service_ongoing_notification_message) + " " + bleManager.getScanner().getCurrentDevices().size());
            mNotificationManager.notify(FOREGROUND_SERVICE_NOTIFICATION_ID, mNotificationBuilder.build());
        }
    };

    @Override
    public void onDeviceLost(BleDevice device) {
        onScanResults();
    }

    @Override
    public void onScanFail(int code) {
        if (bleManager.isScannerLaunched() || bleManager.isAdvertiserLaunched()){
            return;
        }

        if (code == ScanCallback.SCAN_FAILED_APPLICATION_REGISTRATION_FAILED){
            restartBluetooth();
        }

        stopForeground(true);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext());

        notificationBuilder.setContentTitle(getString(R.string.service_scan_fail_notification_message))
                .setTicker(getString(R.string.service_scan_fail_notification_message))
                .setSmallIcon(R.mipmap.ic_launcher);

        mNotificationManager.notify(SCAN_FAILED_NOTIFICATION_ID, mNotificationBuilder.build());
        mNotificationManager.cancel(SCAN_FAILED_NOTIFICATION_ID);
    }

    @Override
    public void onScanStopped() {
        ScanStatusStorage.setScanStatus(this, false);
        restartBluetooth();
        stopForeground(true);
    }

    private void displayOngoingNotification(){

        mNotificationBuilder = new NotificationCompat.Builder(getApplicationContext());

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        mNotificationBuilder.setContentTitle(getString(R.string.service_ongoing_notification_name))
                .setTicker(getString(R.string.service_ongoing_notification_ticker))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentIntent(pendingIntent)
                .setOngoing(true);

        Notification notification = mNotificationBuilder.build();
        startForeground(FOREGROUND_SERVICE_NOTIFICATION_ID, notification);
    }

    @Override
    public void requestDeviceInfo(BleDevice[] addresses) {
        synchronizingDevices.addAll(Arrays.asList(addresses));
        if (synchronizing){
            return;
        }

        requestServerInfo();
    }

    private void requestServerInfo(){
        synchronizing = true;

        List<String> names = new ArrayList<>();
        List<String> uuids = new ArrayList<>();
        List<String> macs = new ArrayList<>();

        synchronized (synchronizingDevices) {

            for (BleDevice device : synchronizingDevices) {
                if (!TextUtils.isEmpty(device.getName())) {
                    names.add(device.getName());
                } else if (device.getUuid() != null) {
                    uuids.add(device.getUuid().toString().toUpperCase());
                } else {
                    macs.add(device.getAddress());
                }
            }
        }

        HttpAdapter.getAdapter().devicesNearby(names.toArray(new String[names.size()]), macs.toArray(new String[macs.size()]), uuids.toArray(new String[uuids.size()]), "match")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new ErrorsSafeSubscriber<UsersNearbyWrapper>() {
                    @Override
                    public void onNext(UsersNearbyWrapper usersNearby) {
                        onServerInfoReceived(usersNearby);
                    }

                    @Override
                    public void onCompleted() {
                        synchronizing = false;
                    }

                    @Override
                    public void onError(Throwable e) {
                        initConnectionForCurrentDevices();
                    }
                });
    }

    private void onServerInfoReceived(UsersNearbyWrapper users){

        bleManager.notifyUsersNearbyList(users.getData());

        synchronized (synchronizingDevices){
            for (UserNearby user: users.getData()){
                bleManager.getScanner().onDeviceInfoReceived(user.getMacAddress(), user.getId(), UUID.fromString(user.getDeviceId()));
                for (BleDevice bleDevice: synchronizingDevices){
                    if (bleDevice.getAddress().equals(user.getMacAddress())){
                        synchronizingDevices.remove(bleDevice);
                    }
                }

            }

            initConnectionForCurrentDevices();
        }
    }

    private void initConnectionForCurrentDevices(){
        String[] macs = new String[synchronizingDevices.size()];
        int i = 0;
        for (BleDevice device: synchronizingDevices){
            macs[i] = device.getAddress();
            i++;
        }
        bleManager.getScanner().connectToDevices(Arrays.asList(macs));
        synchronizingDevices.clear();
    }

    @Override
    public void onCharacteristicRead(String address, UUID uuid) {
        synchronizingDevices.remove(address);

        HttpAdapter.getAdapter().assignAddress(uuid.toString().toUpperCase(), address)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new ErrorsSafeSubscriber<Void>() {

                    @Override
                    public void onNext(Void aVoid) {
                        Log.d("ScannerFragment", "UUID submitted. Address: " + address + " uuid: " + uuid.toString());
                    }
                });
    }

    private void restartBluetooth(){
        bleManager.pauseBle();
        onBleStop();

        BleUtils.getBluetoothAdapter(this).disable();
        forceDisableBluetooth = true;
    }

    private class BluetoothReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(intent.getAction())) {
                int bluetoothState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
                if(bluetoothState == BluetoothAdapter.STATE_ON){
                    if (ScanStatusStorage.getScanLaunchStatus(BleTaskService.this)) {
                        bleManager.resumeBle();
                    }
                } else if(bluetoothState == BluetoothAdapter.STATE_OFF){
                    if (forceDisableBluetooth){
                        forceDisableBluetooth = false;
                        if (!BleUtils.getBluetoothAdapter(BleTaskService.this).enable()){
                            Toast.makeText(BleTaskService.this, "Unable to activate bluetooth", Toast.LENGTH_LONG).show();
                            bleManager.stopBle();
                            onBleStop();
                        }
                    }
                }
            }
        }
    }

    private void onBleStop(){
        handler.removeCallbacks(pingTask);
    }

    private Runnable pingTask = new Runnable(){

        @Override
        public void run() {
            Map<String, BleDevice> devices = new HashMap<>();
            devices.putAll(bleManager.getScanner().getCurrentDevices());
            List<String> names = new ArrayList<>();
            List<String> uuids = new ArrayList<>();
            List<String> macs = new ArrayList<>();
            for (Map.Entry<String, BleDevice> device: devices.entrySet()) {
                BleDevice bleDevice = device.getValue();
                if (!TextUtils.isEmpty(bleDevice.getName())) {
                    names.add(bleDevice.getName());
                } else if (bleDevice.getUuid() != null){
                    uuids.add(bleDevice.getUuid().toString().toUpperCase());
                } else {
                    macs.add(device.getKey());
                }
            }

            HttpAdapter.getAdapter().devicesNearby(names.toArray(new String[names.size()]), macs.toArray(new String[macs.size()]), uuids.toArray(new String[uuids.size()]), "match")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new ErrorsSafeSubscriber<UsersNearbyWrapper>() {
                        @Override
                        public void onNext(UsersNearbyWrapper usersNearbyWrapper) {
                            bleManager.notifyUsersNearbyList(usersNearbyWrapper.getData());
                            Log.d("BleTaskService", "Ping success");
                        }
                    });

            handler.postDelayed(this, PING_FREQUENCY);
        }
    };


    public class BleTaskServiceBinder extends Binder {

        public BleTaskService getService(){
            return BleTaskService.this;
        }
    }

    public static BleTaskService getInstance(){
        return instance;
    }
}
