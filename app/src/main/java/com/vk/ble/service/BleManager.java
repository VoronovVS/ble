package com.vk.ble.service;

import android.content.Context;
import android.location.Location;
import android.support.annotation.Nullable;

import com.vk.ble.CurrentUser;
import com.vk.ble.model.User;
import com.vk.bleapi.UserNearby;
import com.vk.ble.util.OsUtils;
import com.vk.bleapi.BleUtils;
import com.vk.bleapi.advetiser.AdvertiseInfo;
import com.vk.bleapi.advetiser.BleAdvertiser;
import com.vk.bleapi.advetiser.BleAdvertiserListener;
import com.vk.bleapi.scanner.BleScanner;
import com.vk.bleapi.scanner.BleScannerListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 5/9/2016
 * 7:05 PM
 */
public class BleManager {

    private final static String BLE_SERVICE_KEY = "B206EE5D-17EE-40C1-92BA-462A038A33D2";
    private final static String BLE_ID_CHARACTERISTIC = "E669893C-F4C2-4604-800A-5252CED237F9";
    private final static String BLE_NAME_CHARACTERISTIC = "2EFDAD55-5B85-4C78-9DE8-07884DC051FA";
    private final static String BLE_LOCATION_CHARACTERISTIC = "1EA08229-38D7-4927-98EC-113723C30C1B";

    private Context context;
    private BleScanner scanner;
    private BleAdvertiser advertiser;
    private BleCallbacksDelegate callbacksDelegate;

    private List<BleAdvertiserListener> advertiserListeners = new ArrayList<>();
    private List<BleScannerListener> scannerListeners = new ArrayList<>();

    private List<UserNearby> usersNearby;

    public BleManager(Context context, BleScanner scanner, BleAdvertiser advertiser){
        this.context = context;
        this.scanner = scanner;
        this.advertiser = advertiser;

        callbacksDelegate = new BleCallbacksDelegate(this);
        scanner.setListener(callbacksDelegate);
        advertiser.setListener(callbacksDelegate);
        advertiser.setInfo(buildAdvertiseData());

        scanner.setServiceUuidFilter(UUID.fromString(BLE_SERVICE_KEY));
    }

    public BleAdvertiser getAdvertiser() {
        return advertiser;
    }

    public BleScanner getScanner() {
        return scanner;
    }

    public boolean isScannerLaunched(){
        return scanner.isRunning();
    }

    protected void launchBle(){
        if (advertiser.isRunning()) {
            advertiser.stop();
        }

        advertiser.launch();
        scanner.launch();

        for (BleScannerListener listener: getScannerListeners()){
            listener.onScanLaunched();
        }
    }

    public void stopBle(){
        scanner.stop();
        advertiser.stop();
    }

    public void pauseBle(){
        scanner.pause();
        advertiser.stop();
    }

    public void resumeBle(){
        scanner.resume();
        advertiser.launch();
    }

    public boolean isAdvertiserLaunched(){
        return advertiser != null && advertiser.isRunning();
    }

    public void updateAdvertiseData(){
        advertiser.setInfo(buildAdvertiseData());
    }

    private AdvertiseInfo buildAdvertiseData(){
        AdvertiseInfo info = new AdvertiseInfo();
        info.setServiceUuid(UUID.fromString(BLE_SERVICE_KEY));
        info.addServiceData(UUID.fromString(BLE_ID_CHARACTERISTIC), BleUtils.asBytes(getDeviceId()));
        String name = getName();
        info.addServiceData(UUID.fromString(BLE_NAME_CHARACTERISTIC), name.getBytes());
        info.setName(name);
        Location location = OsUtils.getLastKnownLocation(context);
        if (location == null){
            location = new Location("");
            location.setLatitude(0);
            location.setLongitude(0);
        }

        byte[] locationBytes = BleUtils.coordinateAsBytes(location.getLatitude(), location.getLongitude());
        info.addServiceData(UUID.fromString(BLE_LOCATION_CHARACTERISTIC), locationBytes);

        return info;
    }

    private UUID getDeviceId(){
        User user = CurrentUser.getCurrentUser(context);
        if (user == null) {
            throw new AssertionError("Current user is null");
        }
        return UUID.fromString(user.getDeviceId());
    }

    private String getName(){
        User user = CurrentUser.getCurrentUser(context);
        if (user == null) {
            throw new AssertionError("Current user is null");
        }
        return String.valueOf(user.getUserId());
    }

    public void addAdvertiserListener(BleAdvertiserListener listener){
        if (!advertiserListeners.contains(listener)) {
            advertiserListeners.add(listener);
        }
    }

    public void notifyUsersNearbyList(List<UserNearby> users){
        this.usersNearby = users;
        callbacksDelegate.onUsersUpdate(users);
    }

    @Nullable
    public List<UserNearby> getUsersNearby(){
        return usersNearby;
    }

    public void removeAdvertiserListener(BleAdvertiserListener listener){
        advertiserListeners.remove(listener);
    }

    public void addScannerListener(BleScannerListener listener){
        scannerListeners.add(listener);
    }

    public void removeScannerListener(BleScannerListener listener){
        scannerListeners.remove(listener);
    }

    List<BleAdvertiserListener> getAdvertiserListeners(){
        return advertiserListeners;
    }

    List<BleScannerListener> getScannerListeners(){
        return scannerListeners;
    }


}
