package com.vk.ble;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

import com.vk.ble.di.activity.ActivityComponent;
import com.vk.ble.di.activity.ActivityModule;
import com.vk.ble.di.app.ApplicationComponent;
import com.vk.ble.di.app.ApplicationModule;
import com.vk.ble.di.app.DaggerApplicationComponent;
import com.vk.ble.di.service.ServiceComponent;
import com.vk.ble.di.service.ServiceModule;
import com.vk.ble.service.BleTaskService;
import com.vk.ble.storage.ScanStatusStorage;

/**
 * Created by Voronov Viacheslav
 * on 5/4/2016
 * 10:33 PM
 */
public class App extends Application {

    private static Context appContext;

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appContext = getApplicationContext();

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        applicationComponent.inject(this);

        if (ScanStatusStorage.getScanLaunchStatus(this)){
            BleTaskService.initializeService(this);
        }
    }

    public static Context getAppContext(){
        return appContext;
    }

    public ActivityComponent createActivityComponent(Activity activity){
        return applicationComponent.component(new ActivityModule(activity));
    }
    public ServiceComponent createServiceComponent(Service activity){
        return applicationComponent.component(new ServiceModule());
    }

}
