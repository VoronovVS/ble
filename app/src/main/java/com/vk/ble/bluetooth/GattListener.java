package com.vk.ble.bluetooth;

import com.vk.bleapi.BleDevice;

import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 5/2/2016
 * 10:24 PM
 */
public interface GattListener {

    void requestDeviceInfo(BleDevice[] devices);
    void onCharacteristicRead(String address, UUID uuid);

}
