package com.vk.ble.bluetooth;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.vk.bleapi.BleDevice;
import com.vk.bleapi.scanner.BleScanner;

import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 5/2/2016
 * 10:22 PM
 */
public class GattReceiver extends BroadcastReceiver {

    private static final String LOGS = "GattReceiver";

    private GattListener listener;

    public GattReceiver(GattListener listener){
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        switch(intent.getAction()){
            case BleScanner.ACTION_ON_NEW_DEVICES_DISCOVERED:
                Gson gson = new Gson();
                try {
                    BleDevice[] devices = gson.fromJson(intent.getStringExtra(BleScanner.EXTRA_DEVICE_ADDRESSES), BleDevice[].class);
                    onNewDevicesDiscovered(devices);
                }catch (Exception e ){
                    e.printStackTrace();
                }
                break;
            case BleScanner.ACTION_ON_CHARACTERISTIC_READ_SUCCESS:
                onDeviceCharacteristicRead(intent.getStringExtra(BleScanner.EXTRA_DEVICE_ADDRESSES), (UUID) intent.getSerializableExtra(BleScanner.EXTRA_DEVICE_UUID));
        }

    }

    private void onNewDevicesDiscovered(BleDevice[] deviceAddresses){
        if (deviceAddresses == null || deviceAddresses.length == 0){
            Log.e(LOGS, "'GattReceiver.onNewDevicesDiscovered' received empty device address");
            return;
        }

        listener.requestDeviceInfo(deviceAddresses);
    }

    private void onDeviceCharacteristicRead(String deviceAddress, UUID uuid){
        if (TextUtils.isEmpty(deviceAddress) || uuid == null){
            Log.e(LOGS, "'GattReceiver.onDeviceCharacteristicRead' received empty device address or uuid");
            return;
        }

        listener.onCharacteristicRead(deviceAddress, uuid);
    }
}
