package com.vk.ble.network;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import com.vk.ble.R;

import java.net.UnknownHostException;

import rx.Subscriber;

/**
 * Created by Voronov Viacheslav
 * on 1/14/2016
 * 8:56 PM
 */
public abstract class RetrofitSubscriber<T> extends Subscriber<T> {

    private Context context;

    public RetrofitSubscriber(Context context){
        this.context = context;
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();

        AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        dialog.setPositiveButton(context.getString(R.string.ok), (dialog1, which) -> {
            dialog1.dismiss();
        });

        if (e instanceof UnknownHostException) {
            dialog.setMessage("Could not connect to server");
            dialog.show();
            onCompleted();
        } else {
            dialog.setMessage("Error occurred");
            dialog.show();
        }
    }

    @Override
    public void onCompleted() {

    }
}
