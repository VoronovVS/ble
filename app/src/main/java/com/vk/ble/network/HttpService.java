package com.vk.ble.network;

import com.vk.ble.model.UsersNearbyWrapper;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Voronov Viacheslav
 * on 5/3/2016
 * 10:19 AM
 */
public interface HttpService {

    @GET("users_nearby")
    Observable<UsersNearbyWrapper> devicesNearby(
        @Query("user_ids[]") String[] macs,
        @Query("device_ids[]") String[] uuids,
        @Query("user_ids[]") String[] userIds,
        @Query("mode") String mode
    );

    @FormUrlEncoded
    @POST("assign_mac")
    Observable<Void> assignAddress(

        @Field("device_id") String uuid,
        @Field("mac_address") String address
    );
}
