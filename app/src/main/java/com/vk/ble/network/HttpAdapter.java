package com.vk.ble.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vk.ble.App;
import com.vk.ble.CurrentUser;
import com.vk.ble.model.User;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Voronov Viacheslav
 * on 5/3/2016
 * 10:19 AM
 */
public class HttpAdapter {

    private static final String SERVER_URL = "http://bt.b2bmeetup.com/api/bt/";

    private static HttpService service;

    public static HttpService getAdapter() {

        if (service != null){
            return service;
        }

        Gson gson = new GsonBuilder().create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(new AuthInterceptor())
                .build();

        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl(SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        service = retrofit.create(HttpService.class);
        return service;
    }

    private static class AuthInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();
            User user = CurrentUser.getCurrentUser(App.getAppContext());
            if (user != null){
                Log.d("HttpAdapter", "User request: " + user.getUserName() + " token: " + user.getToken());

                Request.Builder requestBuilder = original.newBuilder()
                        .header("X-API-KEY", user.getToken())
                        .method(original.method(), original.body());

                return chain.proceed(requestBuilder.build());
            } else {
                return chain.proceed(original);
            }
        }
    }
}
