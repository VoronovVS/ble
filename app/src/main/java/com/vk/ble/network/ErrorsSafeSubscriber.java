package com.vk.ble.network;

import rx.Subscriber;

/**
 * Created by Voronov Viacheslav
 * on 5/17/2016
 * 9:57 AM
 */
public abstract class ErrorsSafeSubscriber<T> extends Subscriber<T> {

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
    }

}
