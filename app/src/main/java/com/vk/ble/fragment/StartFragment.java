package com.vk.ble.fragment;

import android.bluetooth.le.AdvertiseCallback;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.vk.ble.CurrentUser;
import com.vk.ble.MainActivity;
import com.vk.ble.R;
import com.vk.ble.di.activity.ActivityComponent;
import com.vk.ble.di.fragment.FragmentModule;
import com.vk.ble.model.User;
import com.vk.ble.service.BleManager;
import com.vk.ble.service.BleTaskService;
import com.vk.ble.storage.ScanStatusStorage;
import com.vk.bleapi.BleUtils;
import com.vk.bleapi.advetiser.BleAdvertiserListener;

import java.util.UUID;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;
import butterknife.OnLongClick;

/**
 * Created by Voronov Viacheslav
 * on 4/15/2016
 * 11:01 PM
 */
public class StartFragment extends BaseFragment implements BleAdvertiserListener {

    private static final IntentFilter USER_INTENT_FILTER = new IntentFilter();
    static {
        USER_INTENT_FILTER.addAction(CurrentUser.BROADCAST_ACTION_USER_UPDATE);
    }

    @Bind(R.id.uuidText) TextView uuidText;
    @Bind(R.id.userNameText) TextView userNameText;
    @Bind(R.id.startBleButton) Button launchAdvertiserButton;
    @Bind(R.id.displayBleDevicesList) Button showListButton;

    private UUID currentUuid;

    @Inject
    BleManager bleManager;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_start;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        injectDependencies();

        setProfileInfo();

        if ((Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)){
            uuidText.setVisibility(View.GONE);
            launchAdvertiserButton.setVisibility(View.GONE);
        }

        boolean isRunning = ScanStatusStorage.getScanLaunchStatus(getContext());
        if (isRunning && BleUtils.enabled(getContext())){
            switchToActiveState();
        }
    }

    protected void injectDependencies() {
        ActivityComponent activityComponent = ((MainActivity)getActivity()).getActivityComponent();
        activityComponent.component(new FragmentModule()).inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(userUpdatesReceiver, USER_INTENT_FILTER);
    }

    @Override
    public void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(userUpdatesReceiver);
    }

    @OnClick(R.id.startBleButton)
    void onStartBleButtonClick(){
        boolean isRunning = ScanStatusStorage.getScanLaunchStatus(getContext()) && BleUtils.enabled(getContext());
        if (isRunning){
            stopBleServices();
        } else {
            launchBleServices();
        }
    }

    @OnClick(R.id.displayBleDevicesList)
    void displayBleDevicesListButtonClick(){
        ((MainActivity)getActivity()).displayBleList();
    }

    @OnLongClick(R.id.uuidText)
    boolean onUuidTextPress(){
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("uuid", currentUuid.toString());
        clipboard.setPrimaryClip(clip);
        return true;
    }

    private void launchBleServices(){

        if(BleUtils.enabled(getContext())) {
            BleTaskService.launchService(getContext());
            switchToActiveState();
            bleManager.addAdvertiserListener(this);

        } else {
            ((MainActivity)getActivity()).onBluetoothDisabled(true);
        }
    }

    private void stopBleServices(){

        switchToStoppedState();
        bleManager.addAdvertiserListener(this);

        Intent serviceIntent = new Intent(getContext(), BleTaskService.class);
        serviceIntent.setAction(BleTaskService.SERVICE_STOP);
        getContext().startService(serviceIntent);
    }

    private void switchToActiveState(){
        launchAdvertiserButton.setText(R.string.stop_ble);
        showListButton.setVisibility(View.VISIBLE);
    }

    private void switchToStoppedState(){
        launchAdvertiserButton.setText(R.string.start_ble);
        showListButton.setVisibility(View.GONE);
    }

    @Override
    public void onAdvertiseSuccess() {
        if (isAdded()) {
            Toast.makeText(getActivity(), "Advertiser successfully started", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAdvertiseFail(int errorCode) {
        if (!isAdded()){
            return;
        }

        launchAdvertiserButton.setText(R.string.start_ble);

        String description;
        if (errorCode == AdvertiseCallback.ADVERTISE_FAILED_FEATURE_UNSUPPORTED)
            description = "ADVERTISE_FAILED_FEATURE_UNSUPPORTED";
        else if (errorCode == AdvertiseCallback.ADVERTISE_FAILED_TOO_MANY_ADVERTISERS)
            description = "ADVERTISE_FAILED_TOO_MANY_ADVERTISERS";
        else if (errorCode == AdvertiseCallback.ADVERTISE_FAILED_ALREADY_STARTED){
            if (BleUtils.enabled(getContext())) {
                bleManager.getAdvertiser().stop();
                bleManager.getAdvertiser().launch();
            }
            description = "ADVERTISE_FAILED_ALREADY_STARTED";
        }
        else if (errorCode == AdvertiseCallback.ADVERTISE_FAILED_DATA_TOO_LARGE)
            description = "ADVERTISE_FAILED_DATA_TOO_LARGE";
        else if (errorCode == AdvertiseCallback.ADVERTISE_FAILED_INTERNAL_ERROR)
            description = "ADVERTISE_FAILED_INTERNAL_ERROR";
        else description = "unknown";

        Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();

    }

    private void setProfileInfo(){
        User user = CurrentUser.getCurrentUser(getContext());
        if (user == null) {
            throw new AssertionError("Current user is null");
        }

        currentUuid = UUID.fromString(user.getDeviceId());
        uuidText.setText(user.getDeviceId());
        userNameText.setText(user.getUserName() + " (" + user.getUserId() + ")" );
    }

    private BroadcastReceiver userUpdatesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (CurrentUser.BROADCAST_ACTION_USER_UPDATE.equals(intent.getAction())){
                setProfileInfo();
                bleManager.updateAdvertiseData();
            }
        }
    };
}
