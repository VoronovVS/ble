package com.vk.ble.fragment;

import android.bluetooth.le.ScanCallback;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.vk.ble.MainActivity;
import com.vk.ble.R;
import com.vk.ble.adapter.BleDevicesAdapter;
import com.vk.ble.adapter.UsersNearbyAdapter;
import com.vk.ble.di.activity.ActivityComponent;
import com.vk.ble.di.fragment.FragmentModule;
import com.vk.ble.service.BleManager;
import com.vk.bleapi.BleConstants;
import com.vk.bleapi.BleDevice;
import com.vk.bleapi.UserNearby;
import com.vk.bleapi.scanner.BleScannerListener;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Voronov Viacheslav
 * on 4/18/2016
 * 12:13 AM
 */
public class ScannerFragment extends Fragment implements BleScannerListener {

    @Bind(R.id.list) ListView devicesList;
    @Bind(R.id.users) ListView usersList;

    private BleDevicesAdapter devicesAdapter;
    private UsersNearbyAdapter usersAdapter;

    @Inject
    BleManager bleManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scanner, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        injectDependencies();

        ButterKnife.bind(this, view);

        devicesAdapter = new BleDevicesAdapter();
        usersAdapter = new UsersNearbyAdapter();
        devicesList.setAdapter(devicesAdapter);
        usersList.setAdapter(usersAdapter);
    }

    protected void injectDependencies() {
        ActivityComponent activityComponent = ((MainActivity)getActivity()).getActivityComponent();
        activityComponent.component(new FragmentModule()).inject(this);
    }

    @OnClick(R.id.hideButton)
    void onHideButtonClick(){
        ((MainActivity)getActivity()).hideBleList();
    }

    @Override
    public void onResume() {
        super.onResume();

        usersAdapter.setItems(bleManager.getUsersNearby());
        bleManager.addScannerListener(this);
        devicesAdapter.setItems(bleManager.getScanner().getCurrentDevices());
    }

    @Override
    public void onPause() {
        super.onPause();
        bleManager.removeScannerListener(this);
    }

    @Override
    public void onScanLaunched() {

    }

    @Override
    public void onScanStopped() {

    }

    @Override
    public void onScanUpdate(Map<String, BleDevice> devices) {
        if (isAdded()) {
            getActivity().runOnUiThread(() -> devicesAdapter.setItems(devices));
        }
    }

    @Override
    public void onScanUpdate(BleDevice device) {
        if (isAdded()) {
            getActivity().runOnUiThread(() -> devicesAdapter.updateItem(device));
        }
    }

    @Override
    public void onUsersUpdate(List<UserNearby> users) {
        usersAdapter.setItems(users);
    }

    @Override
    public void onDeviceLost(BleDevice device) {
        if (isAdded()) {
            getActivity().runOnUiThread(() -> devicesAdapter.removeItem(device));
        }
    }

    @Override
    public void onScanFail(int code) {
        String descriptions;
        switch (code){
            case ScanCallback.SCAN_FAILED_ALREADY_STARTED:
                descriptions = "SCAN_FAILED_ALREADY_STARTED";
                break;
            case ScanCallback.SCAN_FAILED_FEATURE_UNSUPPORTED:
                descriptions = "SCAN_FAILED_FEATURE_UNSUPPORTED";
                break;
            case ScanCallback.SCAN_FAILED_APPLICATION_REGISTRATION_FAILED:
                descriptions = "RESTARTING";
                break;
            case ScanCallback.SCAN_FAILED_INTERNAL_ERROR:
                descriptions = "SCAN_FAILED_INTERNAL_ERROR";
                break;
            case BleConstants.BLUETOOTH_IS_DISABLED_ERROR:
                descriptions = "BLUETOOTH_IS_DISABLED_ERROR";
                ((MainActivity)getActivity()).hideBleList();
                break;
            default:
                descriptions = "UNKNOWN_ERROR";
        }
        Toast.makeText(getActivity(), descriptions, Toast.LENGTH_LONG).show();
    }
}
