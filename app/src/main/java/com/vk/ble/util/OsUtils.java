package com.vk.ble.util;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;

import java.util.UUID;

/**
 * Created by Voronov Viacheslav
 * on 4/15/2016
 * 11:12 PM
 */
public class OsUtils {

    public static final int BLE_SHORT_DEVICE_NAME_LENGTH = 12;

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        }
        return manufacturer + " " + model;
    }

    public static String getShortDeviceName(int length){
        String deviceName = OsUtils.getDeviceName();
        if (deviceName.length() > length){
            return deviceName.substring(0, length);
        }
        return deviceName;
    }

    public static Location getLastKnownLocation(Context context){
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
    }
}
